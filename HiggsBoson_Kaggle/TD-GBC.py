# -*- coding: utf-8 -*-
"""
Created on Tue Jul 15 15:48:14 2014

@author: TRIDIB
"""
#%%
import numpy as np
import pandas as pd
import sklearn as sk
import pickle
import math
from sklearn.ensemble import GradientBoostingClassifier as GBC
from sklearn.cross_validation import train_test_split
from sklearn import cross_validation
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.metrics import matthews_corrcoef,roc_auc_score
from pandas import read_csv, DataFrame
import matplotlib.pyplot as plt 
#%%
# load the data from the file training.csv
train_data = read_csv('training.csv')

## Don't need EventId for training
## Don't need weight for training, but need it for AMS score
## convert s to 1 and b to 0 for the training purpose
addCol = train_data['Label'].replace(to_replace=['s','b'], value = [1,0])
train_data['class_int'] = addCol

## definition of AMS score
def AMSScore(s,b): return math.sqrt(2*( (s+b+10.)*math.log(1.+ s/(b+10.))-s) )
    

#=============================================================
#Build a train set and a validation set, xTrain, xVal, yTrain,yVal
# yTrain, and yVal are vectors of response variables:1/0 (s/b)
# wTrain, wVal are the corresponding weights

#xTrain,xVal,yTrain,yVal,wTrain,wVal=train_test_split(train_data.iloc[:,1:31],train_data.iloc[:,33],train_data.iloc[:,31],train_size =0.9, random_state = 123)

skf = cross_validation.StratifiedKFold(train_data.iloc[:,33], n_folds = 2)

#%%
#opt_thVal = [] ## keep track of the data for each fold
amsScoreTrain = []
amsScoreValid = []
accuracyTrain = []
accuracyValid = []
mathewCCTrain = []
mathewCCValid = []
AUCTrain = []
AUCValid = []
thVal = 85
#%%

#%%
for trainIndex, testIndex in skf:
    xTrain = train_data.iloc[trainIndex,1:31]
    xVal   = train_data.iloc[testIndex,1:31]
    yTrain = train_data.iloc[trainIndex,33]
    yVal   = train_data.iloc[testIndex,33]
    wTrain = train_data.iloc[trainIndex,31]
    wVal   = train_data.iloc[testIndex,31]
    #=============================================================

    ## needed for scaling; see later
    rat = float(xTrain.shape[0])/train_data.shape[0] 
    
    ## Now train the classifier
    gbc = GBC(n_estimators=500, learning_rate=0.1 ,max_depth = 5, min_samples_leaf =200, max_features=5, verbose =0)
    gbc.fit(xTrain,yTrain)
    
    #=============================================================
    ## prediction on both xTrain, xVal
    
    prob_predict_train = gbc.predict_proba(xTrain)[:,1]
    prob_predict_valid = gbc.predict_proba(xVal)[:,1]
    
    #=============================================================
    ## Now we compute the AMS score (we want max score)
    
    
    TruePositiveTrain = wTrain*(yTrain == 1.0)*(1/rat)
    TrueNegativeTrain = wTrain*(yTrain == 0.0)*(1/rat)
    TruePositiveValid = wVal*(yVal == 1.0)*(1/(1-rat))
    TrueNegativeValid = wVal*(yVal == 0.0)*(1/(1-rat))
    
      
    pcut = np.percentile(prob_predict_train, thVal)
    
    YhatTrain = prob_predict_train > pcut
    YhatValid = prob_predict_valid > pcut
        
    ## calculate the sTrain, bTrain, sValid, bValid for AMS
    sTrain = sum( TruePositiveTrain*(YhatTrain == 1.0))
    bTrain = sum( TrueNegativeTrain*(YhatTrain == 1.0))
    sValid = sum( TruePositiveValid*(YhatValid == 1.0))
    bValid = sum( TrueNegativeValid*(YhatValid == 1.0))
    amsScoreTrain.append(AMSScore(sTrain,bTrain))
    amsScoreValid.append(AMSScore(sValid,bValid))
    accuracyTrain.append(accuracy_score(yTrain,YhatTrain))
    accuracyValid.append(accuracy_score(yVal,YhatValid))
    mathewCCTrain.append(matthews_corrcoef(yTrain,YhatTrain))
    mathewCCValid.append(matthews_corrcoef(yVal,YhatValid))
    AUCTrain.append(roc_auc_score(yTrain,prob_predict_train))
    AUCValid.append(roc_auc_score(yVal,prob_predict_valid))
  
#==============================================================
#%%
  
#%%
print('AMS Train:',np.mean(amsScoreTrain))
print(np.mean(amsScoreValid))
print(np.mean(accuracyTrain))
print(np.mean(accuracyValid))
print(np.mean(mathewCCTrain))
print(np.mean(mathewCCValid))
print(np.mean(AUCTrain))
print(np.mean(AUCValid))

#%%

## with this optimum value of opt_thVal

#==============================================================================
# pcut = np.percentile(prob_predict_train, thVal)
# 
# #==============================================================
# ## prediction on the test data
# ## test data neither has weight column, nor the label column
# 
#==============================================================================
test_data = read_csv('test.csv')
ids = test_data['EventId']
# # 
xTest = test_data.iloc[:,1:31]
#==============================================================================
pred_val = gbc.predict_proba(xTest)[:,1]
# 
testClassVal = pred_val > pcut
# 
rankOrder = np.argsort(pred_val) + 1 
# 
p = np.empty(len(xTest), dtype = np.object)
# 
p[testClassVal] = 's'
p[~testClassVal] = 'b'
# 
df = pd.DataFrame({"EventId":ids,"RankOrder":rankOrder,"Class":p})
df.to_csv("submission-nEst500.csv", index = False, columns = ["EventId","RankOrder","Class"])
#
# df = pd.DataFrame({"EventId":ids,"RankOrder":rankOrder,"Class":p})
# df.to_csv("submission.csv", index = False, cols = ["EventId","RankOrder","Class"])
#==============================================================================
