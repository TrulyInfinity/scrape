
# coding: utf-8

import pandas as pd
from datetime import datetime
import statsmodels.api as sm
import numpy as np
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from sklearn import cross_validation

#get_ipython().magic(u'matplotlib inline')


#read in data
sf_df = pd.read_csv('Data for Cleaning & Modeling.csv')


#need to convert X11: <1 = 0, 10 or 10+ = 10, 2=2 etc
# if there is 'n/a' entry, we consider it 0 years of employment
def convert(x):
    if x == '< 1 ':
        return 0
    elif x == '10+ ':
        return 10
    elif x == 'n/a':
        return 0
    elif pd.isnull(x):
        x
    else:
        return int(x)

#change X14: VERIFIED-income=V, not Verified = NV, VERIFIED - income source = SV
def changeToShort(x):
    if x == 'VERIFIED - income':
        return 'V'
    elif x == 'not verified':
        return 'NV'
    elif x == 'VERIFIED - income source':
        return 'SV'
    elif pd.isnull(x):
        return x


#Convert X23 to number of DAYS (from the date to today)
#from datetime import datetime

def convert2days(x):
    date_format = "%m/%d/%Y"
    if pd.isnull(x):
        return x
    delta = datetime.today() - datetime.strptime(x,date_format)
    return delta.days

def convert2Other(x):
    if pd.isnull(x):
        return 'OTHER'
    elif x == 'ANY':
        return 'OTHER'
    elif x == 'NONE':
        return 'OTHER'
    else:
        return x

def fillNullByH(x):
    if pd.isnull(x):
        return 'H'
    else:
        return x

def imputeNullByAvg(x,avgMonths):
    if pd.isnull(x):
        return avgMonths
    else:
        return x


#define RMSE
def RMSE(ytest, ypred):
    return np.sqrt(np.mean(ypred-np.log(ytest))**2)

#data cleaning
def cleanData(sf_df):
    #need to change  the following columns (X1,X30) to float
    if sum(sf_df.X1.notnull()) != 0:
        sf_df.X1 = pd.to_numeric(sf_df.X1.str.replace('%',''))

    sf_df.X30 = pd.to_numeric(sf_df.X30.str.replace('%',''))
    #need to change the following columns (X4,X5,X6) to float
    sf_df.X4 = pd.to_numeric(sf_df.X4.str.replace('$','').str.replace(',',''))
    sf_df.X5 = pd.to_numeric(sf_df.X5.str.replace('$','').str.replace(',',''))
    sf_df.X6 = pd.to_numeric(sf_df.X6.str.replace('$','').str.replace(',',''))
    #need to change X7 to only 36 or 60
    sf_df.X7 = pd.to_numeric(sf_df.X7.str.replace('months',''))

    #use the apply function in pandas dataframe
    sf_df.X11 = sf_df.X11.str.replace('years','').str.replace('year','').apply(convert)


    #X14
    sf_df.X14 = sf_df.X14.apply(changeToShort)

    #X23
    sf_df.X23 = sf_df.X23.apply(convert2days)


    return sf_df


FinalDF = cleanData(sf_df)
#now drop certain rows, row index = 364111
FinalDF = FinalDF.drop(FinalDF.index[[364111]])


FinalDF.isnull().sum()

byLoanGrade = FinalDF.groupby('X8')
byHomeOwnership = FinalDF.groupby('X12')
byInitialListingStatus = FinalDF.groupby('X32')


print byHomeOwnership['X5'].sum()/1e6 #converted to millions
print
print
print FinalDF.X12.describe()
print
print FinalDF.X12.value_counts()


def dataPrep(FinalDF):
    #X12
    #given the above information, we will convert ANY and NONE to OTHER
    #given the table above, it seems that NULL values in X12 (~61000 or about 15% of the number of observation) shouldnot
    #be discarded, rather, they can be classified as OTHER

    FinalDF.X12 = FinalDF.X12.apply(convert2Other)


    #X8
    #based on the evidence above, it seems that the NULL values in X8 do contribute substantially to the loan amount funded
    #It seems unreasonable to just get rid off these values. So we encode the NULL values as a seperate LOAN GRADE 'H'

    FinalDF.X8 = FinalDF.X8.apply(fillNullByH)


    #For X25 & X26, WE WILL fill in the null values by average

    avgNumMonthsSinceDelinquency = FinalDF.X25.mean()
    FinalDF.X25 = FinalDF.X25.apply(lambda x: imputeNullByAvg(x, avgNumMonthsSinceDelinquency))

    #Treat X26 as in X25
    avgNumMonthsSinceLastPubRec = FinalDF.X26.mean()
    FinalDF.X26 = FinalDF.X26.apply(lambda x: imputeNullByAvg(x, avgNumMonthsSinceLastPubRec))

    #X13, annual income. We will replace the missing values with median annual income (more robust to outliers)
    medianAnnualIncome = FinalDF.X13.median()
    FinalDF.X13 = FinalDF.X13.apply(lambda x: imputeNullByAvg(x, medianAnnualIncome))

    #X30 replace null by average
    avgRevolveUtilization = FinalDF.X30.mean()
    FinalDF.X30 = FinalDF.X30.apply(lambda x: imputeNullByAvg(x, avgRevolveUtilization))

    #drop the following column from the final dataset (FinalDF)
    dropList = list(FinalDF.columns[[1,2,8,9,14,15,16,17,18]].values)
    FinalDF = FinalDF.drop(dropList, axis=1)

    #Fix the row indices as some of the indices are missing due to above operation
    FinalDF = FinalDF.ix[FinalDF.X1.notnull().tolist(),:]

    FinalDF.index = range(FinalDF.shape[0])
    return FinalDF


#model building

FinalDF = dataPrep(FinalDF)

y = FinalDF.X1
X = FinalDF.iloc[:,1:]



#FinalDF.shape


#one hot encoding of the categorical variables
def oneHotEncoding(X):
    oneHot_X8=pd.get_dummies(X['X8'])
    #drop X8 from X
    X = X.drop('X8', axis =1)
    #attach oneHot_X8
    X = X.join(oneHot_X8)

    #do the same with X12
    oneHot_X12=pd.get_dummies(X['X12'])
    #drop X12 from X
    X = X.drop('X12', axis =1)
    #attach oneHot_X12
    X = X.join(oneHot_X12)



    #do the same with X14 AND DROP X20 (FOR NOW)
    oneHot_X14=pd.get_dummies(X['X14'])
    #drop X14 from X
    X = X.drop('X14', axis =1)
    #attach oneHot_X14
    X = X.join(oneHot_X14)
    #DROP X20 (STATES OF THE BORROWER...WE WILL IGNORE IT FOR OLS)
    X = X.drop('X20', axis = 1)


    #do the same with X32
    oneHot_X32=pd.get_dummies(X['X32'])
    #drop X32 from X
    X = X.drop('X32', axis =1)
    #attach oneHot_X32
    X = X.join(oneHot_X32)
    return X


X = oneHotEncoding(X)
#add constant term
X = sm.add_constant(X)


train=X.sample(frac=0.8,random_state=200)
test=X.drop(train.index)

y_tr = y[train.index]

y_tt = y.drop(train.index)


#simple linear regression
#slightly skewed, so we take log(y_tr)
ln_results = sm.OLS(np.log(y_tr), train).fit()

print ln_results.summary()

# save the model to disk
filename = 'OLS_model_ln.sav'
joblib.dump(ln_results, filename)

# load the model from disk
loaded_ln_model = joblib.load(filename)

y_pred = loaded_ln_model.predict(test)
print 'rmse: ', RMSE(y_pred, np.log(y_tt))


#Ridge Regression
from sklearn.linear_model import Ridge
def ridge_regression(y_tr,train,y_tt,test, alpha):
    #Fit the model
    ridgereg = Ridge(alpha=alpha,normalize=True)
    ridgereg.fit(train,y_tr)
    y_pred = ridgereg.predict(test)
    rmse = RMSE(y_pred, y_tt)
    #Return the result in pre-defined format
    ret = [rmse]
    ret.extend([ridgereg.intercept_])
    ret.extend(ridgereg.coef_)
    return ret



y = FinalDF.X1
X = FinalDF.iloc[:,1:]
X = oneHotEncoding(X)
print X.shape
test_size = 0.33
seed = 4
train, test, y_tr, y_tt = cross_validation.train_test_split(X, y, test_size=test_size, random_state=seed)



alpha_ridge = [1e-4, 1e-3,1e-2, 1,5,10,15,20,25,30]
#Initialize the dataframe for storing coefficients.
#col = ['rmse','intercept'] + ['coef_x_%d'%i for i in range(train.shape[1])]
col = ['rmse','intercept']+X.columns.tolist()
ind = ['alpha_%.2g'%alpha_ridge[i] for i in range(len(alpha_ridge))]
coef_matrix_ridge = pd.DataFrame(index=ind, columns=col)



#train=X.sample(frac=0.8,random_state=200)
#test=X.drop(train.index)
#y_tr = y[train.index]
#y_tt = y.drop(train.index)


for i in range(len(alpha_ridge)):
    coef_matrix_ridge.iloc[i,] = ridge_regression( np.log(y_tr),train,np.log(y_tt),test,alpha_ridge[i])


#choose alpha_30
ridgereg = Ridge(alpha=30,normalize=True)
ridgereg.fit(X,np.log(y))

# save the model to disk
filename = 'ridge_ln.sav'
joblib.dump(ridgereg, filename)

# load the model from disk
loaded_ridgereg = joblib.load(filename)


print HO_DF.shape
print Final_TEST_DF.shape
print Final_TEST.shape


print X_test.shape
print X.shape


HO_DF = pd.read_csv('Holdout for Testing.csv')


Final_TEST_DF = cleanData(HO_DF)


#drop the following column from the final dataset (FinalDF)
dropList = list(Final_TEST_DF.columns[[0,1,2,8,9,14,15,16,17,18]].values)
Final_TEST_DF = Final_TEST_DF.drop(dropList, axis=1)


avgNumMonthsSinceDelinquency = Final_TEST_DF.X25.mean()
Final_TEST_DF.X25 = Final_TEST_DF.X25.apply(lambda x: imputeNullByAvg(x, avgNumMonthsSinceDelinquency))

#Treat X26 as in X25
avgNumMonthsSinceLastPubRec = Final_TEST_DF.X26.mean()
Final_TEST_DF.X26 = Final_TEST_DF.X26.apply(lambda x: imputeNullByAvg(x, avgNumMonthsSinceLastPubRec))


#X30 replace null by average
avgRevolveUtilization = Final_TEST_DF.X30.mean()
Final_TEST_DF.X30 = Final_TEST_DF.X30.apply(lambda x: imputeNullByAvg(x, avgRevolveUtilization))


X_test = oneHotEncoding(Final_TEST_DF)


#There is no H or OTHER SO WE CREATE THOSE IN X_test
X_test['H'] = pd.Series([0]*X_test.shape[0], index = X_test.index)

X_test['OTHER'] = pd.Series([0]*X_test.shape[0], index = X_test.index)

#rearrange the column names of X_test as in X
X_test.columns = X.columns


print X.columns.tolist()
print
print X_test.columns.tolist()


y_pred_ridge_ho = loaded_ridgereg.predict(X_test)


#LASSO
#
from sklearn.linear_model import Lasso
def lasso_regression(y_tr,train,y_tt,test, alpha):
    #Fit the model
    lassoreg = Lasso(alpha=alpha,normalize=True,max_iter=1e5)
    lassoreg.fit(train,y_tr)
    y_pred = lassoreg.predict(test)
    rmse = RMSE(y_pred, y_tt)
    #Return the result in pre-defined format
    ret = [rmse]
    ret.extend([lassoreg.intercept_])
    ret.extend(lassoreg.coef_)
    return ret



y = FinalDF.X1
X = FinalDF.iloc[:,1:]
X = oneHotEncoding(X)

test_size = 0.33
seed = 4
train, test, y_tr, y_tt = cross_validation.train_test_split(X, y, test_size=test_size, random_state=seed)



alpha_lasso = [1e-8,1e-4, 1e-3,1e-2, 1,5,10,15,20,25,30]
#Initialize the dataframe for storing coefficients.
#col = ['rmse','intercept'] + ['coef_x_%d'%i for i in range(train.shape[1])]
col = ['rmse','intercept']+X.columns.tolist()
ind = ['alpha_%.2g'%alpha_lasso[i] for i in range(len(alpha_lasso))]
coef_matrix_lasso = pd.DataFrame(index=ind, columns=col)



for i in range(len(alpha_lasso)):
    coef_matrix_lasso.iloc[i,] = lasso_regression( np.log(y_tr),train,np.log(y_tt),test,alpha_lasso[i])


#coef_matrix_lasso


#GBM
from sklearn import ensemble
test_size = 0.33
seed = 4
train, test, y_tr, y_tt = cross_validation.train_test_split(X, y, test_size=test_size, random_state=seed)

params = {'n_estimators': 50, 'max_depth': 4, 'min_samples_split': 1,
          'learning_rate': 0.01, 'loss': 'ls'}
clf = ensemble.GradientBoostingRegressor(**params)

clf.fit(train, np.log(y_tr))



print RMSE(np.log(y_tt), clf.predict(test))


#choose n_estimator = 20
params = {'n_estimators': 20, 'max_depth': 4, 'min_samples_split': 1,
          'learning_rate': 0.01, 'loss': 'ls'}
clf = ensemble.GradientBoostingRegressor(**params)

clf.fit(X, np.log(y))



# save the model to disk
filename = 'gbm_ln.sav'
joblib.dump(clf, filename)

# load the model from disk
loaded_gbm = joblib.load(filename)
y_pred_gbm = loaded_gbm.predict(X_test)


print len(y_pred_ridge_ho)
print len(y_pred_gbm)


resultDF = pd.DataFrame({'Ridge':y_pred_ridge_ho, 'GBM':y_pred_gbm})


resultDF.to_csv('Results From Tridib Dutta.csv', header = True, index= True)
