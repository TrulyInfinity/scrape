## this modules contains all the function that are used in Main.py
## modules used
## Author: TRIDIB Dutta 
import requests
from BeautifulSoup import BeautifulSoup
import pandas as pd
import re
import datetime
import os, sys
import pyodbc
import subprocess



#### functions to be used in Main.py##################
def generateDateToColNumMap(startYear,startMonth,endYear,endMonth):
    map_vals = dict()
    years = list(range(startYear,endYear+1,1))
    for year in years:
        if startYear == endYear:
            months = range(startMonth,endMonth+1,1)
        else:
            if year == startYear:
                months = range(startMonth,13,1)
            elif year == endYear:
                months  = range(1,endMonth+1,1)
            else:
                months = range(1,13,1)
        for month in months:
            if  month <= 12:
                if year < 2009:
                    if year != 2008:
                        map_vals[str(year)+'_'+ str(month)] = 4
                    else:
                        if month < 9:
                            map_vals[str(year)+'_'+ str(month)] = 4
                        else:
                            map_vals[str(year)+'_'+ str(month)] = 5
                elif year >= 2009 and year < 2013:
                    if year != 2012:
                        map_vals[str(year)+'_'+ str(month)] = 5
                    else:
                        if month < 10:
                            map_vals[str(year)+'_'+ str(month)] = 5
                        else:
                            map_vals[str(year)+'_'+ str(month)] = 7
                else:
                    if year != endYear:
                        map_vals[str(year)+'_'+ str(month)] = 7
                    else:
                        if month < (endMonth +1):
                            map_vals[str(year)+'_'+ str(month)] = 7
    return map_vals



def generateURL(month,year):
    url = ''
    if month < 10:
        strMonth = '0'+str(month)
    else:
        strMonth = str(month)
    url = 'http://www.neo.ne.gov/statshtml/122/'+str(year)+'/122_'+str(year)+strMonth+'.htm'
    return url

def getFIPSdata(workingDir):
    fips = pd.read_excel( workingDir +'fips_codes_website.xls')

    fips.columns = ['state','state_fips','county_fips','fips_entity', 'ANSI_code','city','entity_description']
    return fips

def StateFips(location, fips):
    if sum((fips['city'] ==location[0].strip()) & (fips['state'] == location[1].strip())) !=0:
        return fips.ix[(fips['city'] ==location[0].strip()) & (fips.state == location[1].strip()),1:3].state_fips.values.tolist()[0]
    else:
        return fips.ix[fips.state==location[1].strip(),['state_fips']].state_fips.tolist()[0]

def CountyFips(location, fips):
    if sum((fips['city'] ==location[0].strip()) & (fips['state'] == location[1].strip())) !=0:
        return fips.ix[(fips['city'] ==location[0].strip()) & (fips.state == location[1].strip()),1:3].county_fips.values.tolist()[0]
    else:
        return 'NA'


## now run the database query on the historic data to find out what are the latest year and month
def query(db, schema, sqlServerInstance, workingDir,tableToWriteTo):
    connStr = "DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection = Yes"
    y=0
    m=0
    conn = pyodbc.connect(connStr)
    cursor = conn.cursor()
    query = "SELECT TOP 1 month, year FROM ["+db+"].["+schema+"].["+tableToWriteTo+"] ORDER BY year desc, month desc" ## enter the query to be executed here
    cursor.execute(query)
    for row in cursor:
        y = row[1]
        m = row[0]

    ## close the connection once we get the values
    conn.close()
    return [y, m]

## get the links from the archive page to find the latest update
def getURLcontents():
    url = 'http://www.neo.ne.gov/statshtml/122_archive.htm'
    r = requests.get(url)
    return r

## scrape the table of links in this page
def getCurrYrFromArchive(r): # r is the output of getURLcontents
    bs = BeautifulSoup(r.text)

    trs = bs.findAll('tr')

    ths = trs[1].findAll('th')
    currYrArchive = int(trs[1].findAll('th')[0].text)
    return currYrArchive

## since we are only reading in the top row, we need only this row's tds
def getMonthsForCurrYear(r): # r is the output of getURLcontents
    monthsFromTopRow = []
    calender = {'Jan':1,'Feb': 2, 'Mar': 3, 'Apr':4, 'May': 5,
                'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep':9, 'Oct':10,
                'Nov':11,'Dec':12}
    bs = BeautifulSoup(r.text)
    trs = bs.findAll('tr')
    tds = trs[1].findAll('td')
    for td in tds:
        if td.text !='NA':
            print calender[td.text]
            monthsFromTopRow.append(calender[td.text])

    return monthsFromTopRow

def getMonthsForPrevYear(r): # r is the output of getURLcontents
    monthsFromSecRow = []
    calender = {'Jan':1,'Feb': 2, 'Mar': 3, 'Apr':4, 'May': 5,
                'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep':9, 'Oct':10,
                'Nov':11,'Dec':12}
    bs = BeautifulSoup(r.text)
    trs = bs.findAll('tr')
    tds = trs[2].findAll('td')
    for td in tds:
        if td.text !='NA':
            print calender[td.text]
            monthsFromSecRow.append(calender[td.text])

    return monthsFromSecRow

## scrape(..) is the workhorse which scrapes the table from the url provided
def scrape(url, fips, year, month, map_vals_key):
    non_decimal = re.compile(r'[^\d.]+') ## numeric, and decimal point will be kept when re.sub is used
    DF = pd.DataFrame()

    ## initialize the required lists which will be used as Series to build pandas data frame
    state_company = []
    city = []  ## location col is split into two: city, state
    state = [] ## location col is split into two: city, state
    Nameplate_Capacity = []
    Operating_Production = []
    UnderConstruction = []
    state_fips = []
    county_fips = []

    r = requests.get(url)

    bs = BeautifulSoup(r.text)

    trs = bs.findAll('tr') ## get all the trs (a.k.a rows) of the table
    ## go through each row in a for loop
    #print len(trs)
    for tr in trs:
        tds = tr.findAll('td') ## get all the tds (a.k.a fields) for the this row
        n = len(tds)
        if n == map_vals_key:
            if map_vals_key == 4: ## only 4 cols, no Operating_Production col
                #print (tds[0].text, tds[1].text, tds[2].text, tds[3].text)
                state_company.append(tds[0].text.replace(',','')) ## company name, 1st col
                location = tds[1].text.split(',') ## 2nd col, location split into two
                #print location[0],' ',location[1]
                if len(location) == 2:
                    city.append(location[0].strip())
                    state.append(location[1].strip())
                else:
                    city.append('NA')
                    state.append(tds[1].text)

                ## update the state_fips and county_fips
                if len(location) == 2:
                    temp_st_fips = StateFips(location,fips)
                    temp_county_fips = CountyFips(location, fips)
                    state_fips.append(temp_st_fips)
                    county_fips.append(temp_county_fips)
                else:
                    county_fips.append('NA')
                    state_fips.append('NA')


                if tds[2].text != 'NA': ## 3rd col, Nameplate_Capacity
                    Nameplate_Capacity.append(float(non_decimal.sub('',tds[2].text))) ## * are removed if present
                else:
                    Nameplate_Capacity.append(0)

                ## since no Operating_Production col exists, we assume it is same as Nameplate_Capacity
                if tds[2].text != 'NA': ## 3rd col, Nameplate_Capacity
                    Operating_Production.append(float(non_decimal.sub('',tds[2].text))) ## * are removed if present
                else:
                    Operating_Production.append(0)


                if tds[3].text != 'NA': ## 4th col, under construction capacity
                    UnderConstruction.append(float(non_decimal.sub('',tds[3].text)))## * are removed if present
                else:
                    UnderConstruction.append(0)

            elif map_vals_key == 5:
                state_company.append(tds[0].text.replace(',','')) ## company name, 1st col
                location = tds[1].text.split(',') ## 2nd col, location split into two
                if len(location) == 2:
                    city.append(location[0].strip())
                    state.append(location[1].strip())
                else:
                    city.append('NA')
                    state.append(tds[1].text)

                 ## update the state_fips and county_fips
                if len(location) == 2:
                    temp_st_fips = StateFips(location, fips)
                    temp_county_fips = CountyFips(location, fips)
                    state_fips.append(temp_st_fips)
                    county_fips.append(temp_county_fips)
                else:
                    county_fips.append('NA')
                    state_fips.append('NA')


                if tds[2].text != 'NA': ## 3rd col, Nameplate_Capacity
                    Nameplate_Capacity.append(float(non_decimal.sub( '',tds[2].text))) ## * are removed if present
                else:
                    Nameplate_Capacity.append(0)


                ## For year 2008, months = 10,11,12,
                ##Underconstruction = tds[3].text + tds[4].text
                ## Operating_Production = Nameplate_Capacity
                if year == 2008 and (month == '10' or month == '11' or month == '12'):
                    ## since no Operating_Production col exists, we assume it is same as Nameplate_Capacity
                    if tds[2].text != 'NA': ## 3rd col, Nameplate_Capacity
                        Operating_Production.append(float(non_decimal.sub('',tds[2].text))) ## * are removed if present
                    else:
                        Operating_Production.append(0)

                    if tds[3].text != 'NA':
                        UC1 = float(non_decimal.sub('',tds[2].text))
                    else:
                        UC1 = 0
                    if tds[4].text != 'NA':
                        UC2 = float(non_decimal.sub('',tds[4].text))
                    else:
                        UC2 = 0
                    UnderConstruction.append(UC1 + UC2)
                else: ## for other years, it is normal
                    if tds[3].text != 'NA': ## 4th col, operating production
                        Operating_Production.append( float(non_decimal.sub('',tds[3].text)) )
                    else:
                        Operating_Production.append(0)

                    if tds[4].text != 'NA': ## 5th col, Under construction/expansion capacity
                        UnderConstruction.append( float(non_decimal.sub('',tds[4].text)))
                    else:
                        UnderConstruction.append(0)
            elif map_vals_key == 7:
                state_company.append(tds[0].text.replace(',','')) ## company name, 1st col
                location = tds[1].text.split(',') ## 2nd col, location split into two
                if len(location) == 2:
                    city.append(location[0].strip())
                    state.append(location[1].strip())
                else:
                    city.append('NA')
                    state.append(tds[1].text)

                 ## update the state_fips and county_fips
                if len(location) == 2:
                    temp_st_fips = StateFips(location, fips)
                    temp_county_fips = CountyFips(location, fips)
                    state_fips.append(temp_st_fips)
                    county_fips.append(temp_county_fips)
                else:
                    county_fips.append('NA')
                    state_fips.append('NA')


                if tds[2].text != 'NA': ## 3rd col, Nameplate_Capacity
                    Nameplate_Capacity.append(float(non_decimal.sub( '',tds[2].text))) ## * are removed if present
                else:
                    Nameplate_Capacity.append(0)

                if tds[3].text != 'NA': ## 4th col, operating production
                    Operating_Production.append( float(non_decimal.sub('',tds[3].text)) )
                else:
                    Operating_Production.append(0)

                if tds[4].text != 'NA': ## 5th col, Under construction/expansion capacity
                    UnderConstruction.append( float(non_decimal.sub('',tds[4].text)))
                else:
                    UnderConstruction.append(0)


    ## build the data frame
    data = {'a':state_company,
            'b': city,
            'c': state,
            'd':Nameplate_Capacity,
            'e':Operating_Production,
            'f':UnderConstruction,
            'g':state_fips,
            'h':county_fips}


    DF = pd.DataFrame(data)
    ## Add Year and Month columns to it so that we can distinguish when added to a larger data table
    DF.loc[:,'Year'] = pd.Series([year]*len(DF), dtype='int' ,index = DF.index)
    DF.loc[:,'Month'] = pd.Series([month]*len(DF), dtype='int' ,index = DF.index)

    ## rename the columns
    columns = ['company','cityName','stateName','Nameplate_Capacity','Operating_Production','Capacity_UnderConstruction','state_fips',
              'county_fips','Year','Month']

    DF.columns = columns
    ## there are some problems due to encoding. '-' has been converted to '&ndash;', and '&' to '&amp;'
    ## we will clean it up next
    DF.company = DF.ix[:,0].str.replace('&ndash;','-')

    ## replace '&amp;' by '&'
    DF.company = DF.ix[:,0].str.replace('&amp;','&')

    ## rearrange the columns
    DF = DF[['state_fips','county_fips','Year','Month','company', 'cityName','stateName',
            'Nameplate_Capacity','Operating_Production','Capacity_UnderConstruction']]

    print DF.shape #for sanity check
    return DF

## getData(...) is the driver function.It generates url,runs scrape() and writes the generated data to the folder
def getData(startYear, startMonth, endYear, endMonth,workingDir):
    fips = getFIPSdata(workingDir)
    map_vals = generateDateToColNumMap(startYear,startMonth,endYear,endMonth)
    print len(map_vals)
    #raw_input("HIT ENTER")
    df = pd.DataFrame()
    for key in map_vals.keys():
        temp_key = key.split('_')
        year = int(temp_key[0])
        month = int(temp_key[1])
        print year," ",month," ",map_vals[key]
        url = generateURL(month,year)
        print url
        ## now scrape
        DF = scrape(url, fips, year, month,map_vals_key=map_vals[key])
        df = pd.concat([df,DF], ignore_index=True)
    return df
        ## write this to a file rather than returning
        #df.to_csv(FileToWriteTo, index = False)

#############################################################################
## Loading functions ##
def FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload,tableToWriteTo ):
    conn = pyodbc.connect("DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection= Yes")
    cursor = conn.cursor()
    sqlcmd_one = """CREATE TABLE [dbo].["""+tableToWriteTo+"""]  (
    [state_fips] bigint,
    [county_fips] bigint,
    [Year] bigint,
    [Month] bigint,
    [company] varchar(max),
    [cityName] varchar(max),
    [stateName] varchar(50),
    [Nameplate_Capacity] float,
    [Operating_Production] float,
    [Capacity_UnderConstruction] float
    )"""
    sqlcmd_two ="""TRUNCATE TABLE [dbo].["""+tableToWriteTo+"""]"""

    try:
        sqlcmd = sqlcmd_one
        cursor.execute(sqlcmd)
        cursor.commit()
        conn.close()
        print 'Initial create of ['+tableToWriteTo+'] Complete.'
    except Exception as e:
        sqlcmd = sqlcmd_two
        cursor.execute(sqlcmd)
        cursor.commit()
        conn.close()
    ############ use bulk copy (bcp) to load the data into the CommodityFutures table
    theproc = subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + FileToUpload + ' -c -t,  -T -S' + sqlServerInstance)


def loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir, UpdateFileToUpload, tableToWriteTo):
    if os.path.isfile(workingDir + UpdateFileToUpload):
        subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + UpdateFileToUpload + ' -c -t,  -T -S' + sqlServerInstance)
        print 'New Data is uploaded'
    else:
        print "No New Data"
##############################################################################


if __name__ == '__main__':
    getData(startYear, startMonth, endYear, endMonth)
    FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload,tableToWriteTo)
    loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir,UpdateFileToUpload, tableToWriteTo)
