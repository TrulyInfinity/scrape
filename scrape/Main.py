#################################################################
## This script is the main script
## For initial upload (to build the historical data)
## Author: TRIDIB Dutta 
#### python Main.py I/i
## For updates
###  python Main.py U/u
##################################################################

# modules required
import requests
from BeautifulSoup import BeautifulSoup
import pandas as pd
import re
import datetime
import os, sys
import pyodbc
import ScrapingEthanol

## set the global variables here
workingDir = "E:\\DatabaseFiles\\UpdateDBFiles\\EthanolPlants\\"
#sqlServerInstance = 'AG-AEM-1M9RBY1'
sqlServerInstance = ".\mssqlsvrag"
#sqlServerInstance = ".\MSDEV"
schema = 'dbo'
db = 'AgDB'

FileToWriteTo = 'EthanolPlants.csv'
FileToWriteToUpdate = 'New_EthanolPlantData.csv'
tableToWriteTo = 'EthanolPlants'
FileToUpload = FileToWriteTo
UpdateFileToUpload = FileToWriteToUpdate


def main(argv): ## argv is either I/i or U/u
    try:
    	os.chdir(workingDir)
        if not argv or len(argv) !=1:
            print 'Wrong Command: Enter I or i for initial load or U or u for update'
            sys.exit(1)

        loadFlag = argv[0]
        print loadFlag
        if loadFlag == 'I' or loadFlag == 'i':
            print 'getting historical data'
            a = ScrapingEthanol.getData(2005,1,2015,12,workingDir)
            a.to_csv(FileToWriteTo, index = False)
            print "Historical data is written in the working dir to a file: ", FileToWriteTo
            print 'Loading historical data to DB'
            ScrapingEthanol.FlatFileToUpload(db, schema, sqlServerInstance, workingDir,FileToUpload,tableToWriteTo)
            print 'Historical data loaded to DB'

        elif loadFlag == 'U' or loadFlag == 'u':
            r = ScrapingEthanol.getURLcontents()
            currYrArchive = ScrapingEthanol.getCurrYrFromArchive(r)
            monthsFromTopRow = ScrapingEthanol.getMonthsForCurrYear(r)
            if len(monthsFromTopRow) == 0:
                currYrArchive = currYrArchive - 1
                monthsFromSecRow = ScrapingEthanol.getMonthsForPrevYear(r)
                currMonthArchive = monthsFromSecRow[len(monthsFromSecRow)-1]
            else:
                monthsFromTopRow = ScrapingEthanol.getMonthsForCurrYear(r)
                currMonthArchive = monthsFromTopRow[len(monthsFromTopRow)-1]

            print "Got currMonthArchive"
            print 'running DB query to get lastUpdateDBYear and lastUpdateDBMonth'
            [lastUpdateDBYear, lastUpdateDBMonth] = ScrapingEthanol.query(db, schema, sqlServerInstance, workingDir,tableToWriteTo)
            print lastUpdateDBYear
            ## getNewData() will scrape the new data and write it to Monthlyfile = 'New_EthanolPlantData.csv' in the workingDir
            print "getting new data"
            a = ScrapingEthanol.getData(lastUpdateDBYear,lastUpdateDBMonth+1,currYrArchive,currMonthArchive,workingDir)
            if len(a) > 0:
                a.to_csv(FileToWriteToUpdate, index = False)
                print "new updates are written in the working directory to the file: ", FileToWriteToUpdate
                ##load new data to DB. Note: the tableToWriteTo='EthanolPlants' is already defined in the ScrapingEthanol.py script
                print "uploading new data to the specified DB"
                ScrapingEthanol.loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir,UpdateFileToUpload, tableToWriteTo)
                ## remove the csv file
                os.remove(FileToWriteToUpdate)
            else:
                print "No new data"
        else:
            print 'Invalid command argument'
            print 'Enter I or i for initial scraping and upload, U or u for updating the existing data by new scraped data'

    except Exception, e:
        print "General Exception"
        print e
        #raise Exception('force to fail')
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv[1:])
