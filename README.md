# README #

# Scrape #
This folder contains scraping a websites to obtain raw data.

## Note1
`GECrops.py` is another scraping job (It downloads a file, process and format it and then uploads to a SQL server)

## Note2
`ProjStFa.py` is a practice project which I worked on based on some data I got. It basically contains some cleaning, feature engineering and few basic modeling efforts. This is for practice purpose

## Note3
`scrapeGraintTransportByMode.py` is another scraping routine

## Note4
`myScript.R` is a R script containing data preperation for a project. This is the first iteration (subsequent iterations has been ported to MatLab)

## Note5
`AnalyticsEdge....Rmd` is R markdown document which contains my first attempt at model building and taking part in Kaggle competition, as part of the MOOC course 'Analytic's Edge'. *There may be may mistakes contained in it. I have learnt a lot from this. Particularly about over fitting.*

## Note6
`CrowdAnalytix...Rmd` is a R markdown document containing my effort in building a predictive model for housing occupancy in the US housing sector based on historical data. *This was also done a while back when I was feeling my way around in model builidng and analysis*

# NOTICE #
THIS FILES ARE PROVIDED ONLY AS AN EXAMPLE CODE FOR MY PORTFOLIO. NOT FOR GENERAL USE.
