import os
import popen2
import requests
from lxml import html
import urllib2
import shutil
import us
from lxml import etree
import us

#this function is to load one specific URL and downlaod the URL for speficific state and year and workingDir
#the parameters we need to set is workingDir and state and year for one specific URL
def main(workingDir, state, year):

    #set the path for where the downloaded file should be
    #downLoadPath = workingDir + "staging//"
    downLoadPath = workingDir + "cdl_staging//"
    if not os.path.exists(downLoadPath):
        os.mkdir(downLoadPath)

    #this is the prefix the downloaded URL
    urlPartial = 'http://nassgeodata.gmu.edu:8080/axis2/services/CDLService/GetCDLFile?'

    #here is the specific part for some URLs
    stateupperCase = state.upper()
    StateFIPS = us.states.lookup(stateupperCase).fips

    urlspec = "year=" + str(year) + '&fips=' + str(StateFIPS)
    url = urlPartial + urlspec
    r = requests.get(url)

    #Get the specific link in that website
    tree = html.fromstring(r.text)
    subaList = tree.xpath("//text()")
    print subaList
    link = subaList[0]
    print link


    #Here is the code to download some layer
    try:
        req =urllib2.urlopen(link)
    except:
        #print "Error: The CDL data is not available within the area of 27 for the year of 2000."
        return []
    downLoadFileName = "CDL_"+ str(year) + "_" + str(StateFIPS) + ".tif"
    with open(downLoadPath + downLoadFileName, 'wb') as fp:
        shutil.copyfileobj(req, fp)
        # for chunk in req.iter_content(chunk_size=1024):
        #     if chunk:
        #         fp.write(chunk)
        #         fp.flush()
    fp.close()
    print "finish downloading"

    #return the file name
    return downLoadFileName