import zipfile
import os
import pickle 
import arcpy 
from time import sleep 
import downloadOneURL_cdl



def uncompressNDVItifFile(zipFile, destinationFolder):
    #zipFile is the path to the .zip file
    #extract in the destinationFolder
    #returns the title name of the extracted NDVI tif file and status
    zifref = zipfile.ZipFile(zipFile,'r')
    fileList = zifref.namelist()
    for file in fileList:
        aList = file.split('.')
        if aList[4].split('_')[1] == 'NDVI' and aList[-1] == 'tif':
            name = '.'.join(aList)
            zifref.extract(name, destinationFolder)
            sleep(5)
            zifref.close()
            return [name,1]
    zifref.close()
    return ['NDVI tif file not found',0]

with open('URLS.txt', 'r') as handle:
    URLS = pickle.load(handle)
handle.close()

url = URLS[0]
fileName = url.split("/")[-1]
workingDir = os.getcwd() + "\\"
print workingDir
zipFile = workingDir + "staging\\" + fileName

[emodisFileName, status] = uncompressNDVItifFile(zipFile, workingDir+"staging")
print emodisFileName
print status 

#run the following codes to set up arcpy environment
arcpy.env.workspace = workingDir
arcpy.env.overwriteOutput=True
arcpy.CheckOutExtension("Spatial")

# Add 5 digit FIPS code to shape file
#add field for 5 digit fips code = stateFIPS*1000+countyFIPS. Note, already added but in original census files have to run the 2 lines below to get 5 digit fips
arcpy.AddField_management(in_table= workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp",field_name="FIPS",field_type="LONG")
arcpy.CalculateField_management(in_table= workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp",field="FIPS",expression="int( !STATEFP!)*1000+int( !COUNTYFP!)",expression_type="PYTHON_9.3")


ndvi_tiff_file = workingDir + "staging\\" + emodisFileName
arcpy.gp.SetNull_sa(ndvi_tiff_file, ndvi_tiff_file, workingDir + "staging\\"+"newTiff.tif" , """"Value" =-2000""")
# state = 'IL'
# year = 2000 ## year range should be 2000 to 2014
# crop = 'Corn'

# """Download the CDL file for the state and year"""
# # cdlName = downloadOneURL_cdl.main(workingDir, state, year)
# # print cdlName 

cdlName = "CDL_2000_17.tif"
cdlpath = workingDir + "staging\\"+ cdlName
print cdlpath
print
new_ndvi_tiff_file = workingDir + "staging\\newTiff.tif" 
print new_ndvi_tiff_file
print 
output_con_raster = workingDir + "staging\\con_" + cdlName
print output_con_raster
print 

try:
    arcpy.gp.Con_sa(workingDir+"staging\\"+cdlName, new_ndvi_tiff_file, output_con_raster,"", """"Value" = %d""" % 1)
except:
    print arcpy.GetMessages()
# #arcpy.gp.Con_sa(cdlpath, new_emodis_file, output_con_raster, "Value = %d" % (cropCodes[crop],))
# arcpy.gp.Con_sa(cdlpath, new_emodis_file, output_con_raster, """"Value" = 1""" )
# #print "conditional raster done"
# try:
#     print "RUN con_sa"
#     arcpy.gp.Con_sa("C:/Users/td276/Dropbox/AgDB_Admin/CDL_new/staging/CDL_2010_27.tif", "C:/Users/td276/Dropbox/AgDB_Admin/CDL_new/staging/newTiff.tif", "C:/Users/td276/Dropbox/AgDB_Admin/CDL_new/staging/Con_tif2", "", """"Value" = 1""")
#     print "done"
# except:
#     print arcpy.GetMessages()