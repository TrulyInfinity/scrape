"""
Author: TRIDIB DUTTA 
Script: This script computes the average reflective index condition upon the crop layer for each statee for each year starting from 2000
"""


""" Below are the main steps (as I see now) needed """

"""
step 1. get the urls (use previous code now)
step 2. download the crops raster files (tiff images) from gmu site (use previous code for now)
step 3. get the emodis data (NDVI) as in EMODIS job
setp 3a. May need to use SetNULL_sa() to remove some of discrepencies as in EMODIS job
step 4. we have to find the ndvi values for the pixels which is conditioned upon the crop values in the crop raster files i,e when we are working on say 'Corn', we need find out the NDVI values for pixels which is represents 'Corn'
step 5. step 4. is obtained by generating conditional raster data (a new tiff I pressume) using arcpy's con_sa() function
step 6. Once we have the new raster data which contains ndvi values corresponding to the pixels for which 'Corn' is 'Yes'. Otherwise the values are null or NoData
step 7. With the new raster file, proceed as in case of emodis job and find the mean  

"""


""" Import the following modules and .py files """

import downloadOneURL_cdl #as in emodis
import downloadOneURL_emodis #same as in emodis
import getAllURLS

import us
import os, sys 
import pandas as pd 
import pyodbc 
import subprocess
import csvkit 
import arcpy 
import pickle 
import zipfile
import glob
import datetime
import numpy as np 
from time import sleep


""" The following can be reliably obtained from the module us 
states = ['al', 'ar', 'az', 'ca', 'co', 'ct', 'de', 'fl', 'ga', 'id', \
          'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', \
          'mn', 'ms', 'mo', 'mt', 'ne', 'nh', 'nj', 'nm', 'ny', 'nv', \
          'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', \
          'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy']

stateFIPS = {'AL':'01', 'AZ':'04', 'AR':'05', 'CA':'06', 'CO':'08', \
             'CT':'09', 'DE':'10', 'FL':'12', 'GA':'13', 'ID':'16', \
             'IL':'17', 'IN':'18', 'IA':'19', 'KS':'20', 'KY':'21', \
             'LA':'22', 'ME':'23', 'MD':'24', 'MA':'25', 'MI':'26', \
             'MN':'27', 'MS':'28', 'MO':'29', 'MT':'30', 'NE':'31', \
             'NV':'32', 'NH':'33', 'NJ':'34', 'NM':'35', 'NY':'36', \
             'NC':'37', 'ND':'38', 'OH':'39', 'OK':'40', 'OR':'41', \
             'PA':'42', 'RI':'44', 'SC':'45', 'SD':'46', 'TN':'47', \
             'TX':'48', 'UT':'49', 'VT':'50', 'VA':'51', 'WA':'53', \
             'WV':'54', 'WI':'55', 'WY':'56'}
"""

stateFIPS = dict()
for i in range(len(us.STATES)):
	stateFIPS[str(us.STATES[i].abbr)] = str(us.STATES[i].fips)

#print stateFIPS


## following is the dictionary for the crop
## the key is the crop; the value is the code for the corresponding color in the crop raster
cropCodes = {"Corn":1, "Cotton":2, "Rice":3, "Sorghum":4, "Soybeans":5, \
             "Sunflower":6, "Peanuts":10, "Tobacco":11, "SweetCorn":12, \
             "PopOrOrnCorn":13, "Mint":14, "Barley":21, "DurumWheat":22, \
             "SpringWheat":23, "WinterWheat":24, "OtherSmallGrains":25, \
             "WinterWheat-Soybeans":26, "Rye":27, "Oats":28, "Millet":29, \
             "Speltz":30, "Canola":31, "Flaxseed":32, "Safflower":33, \
             "RapeSeed":34, "Mustard":35, "Alfalfa":36, "OtherHay":37, \
             "Camelina":38, "Buckwheat":39, "Sugarbeets":41, "DryBeans": 42, \
             "Potatoes":43, "OtherCrops":44, "Sugarcane":45, \
             "SweetPotatoes":46, "MiscVegsFruits":47, "Watermelons":48, \
             "Onions":49, "Cucumbers":50, "ChickPeas":51, "Lentils":52, \
             "Peas":53, "Tomatoes":54, "Caneberries":55, "Hops":56, \
             "Herbs":57, "Clover-Wildflowers":58, "Sod-GrassSeed":59, \
             "Switchgrass":60, "Cherries":66, "Peaches":67, "Apples":68, \
             "Grapes":69, "ChristmasTrees":70, "OtherTreeCrops":71, \
             "Citrus":72, "Pecans":74, "Almonds":75, "Walnuts":76, \
             "Pears":77, "Pistachio":204, "Triticale":205, "Carrots":206, \
             "Aparagus":207, "Garlic":208, "Cantalupes":209, "Prunes":210, \
             "Olives":211, "Oranges":212, "HoneydewMelons":213, \
             "Broccoli":214, "Peppers":216, "Pmegranates":217, \
             "Nectarines":218, "Greens":219, "Plums":220, "Strawberries":221, \
             "Squash":222, "Apricots":223, "Vetch":224, \
             "WinterWheat-Corn":225, "Oats-Corn":226, "Lettuce":227, \
             "Pumpkins":229, "Lettuce-DurumWheat":230, \
             "Lettuce-Canteloupe":231, "Lettuce-Cotton":232, \
             "Lettuce-Barley":233, "DurumWheat-Sorghum":234, \
             "Barley-Sorghum":235, "WinterWheat-Sorghum":236, \
             "Barley-Corn":237, "WinterWheat-Sorghum":238, \
             "Soybeans-Cotton":239, "Soybeans-Oats":240, \
             "CropCorn-Soybeans":241, "Blueberries":242, "Cabbage":243, \
             "Cauliflower":244, "Celery":245, "Radishes":246, "Turnips":247, \
             "EggPlants":248, "Gourds":249, "Cranberries":250, \
             "Barley/Soybeans":254}


FileToWriteTo = 'CDL_ndvi.csv'
FileToWriteToUpdate = 'CDL_ndvi_update.csv'
tableToWriteTo = 'CDL_ndvi'
FileToUpload = FileToWriteTo
UpdateFileToUpload = FileToWriteToUpdate
#pythonpath="C:\\Python27\\ArcGISx6410.4\\Scripts\\"
#need to update global in ScapingEMODIS file too
#global variables for SQL server instance
sqlServerInstance = '.\MSDEV' #check
#sqlServerInstance ="AG-AEM-1M9RBY1"
#sqlServerInstance ="PUGET-107431"
db = 'AgDB'
schema = 'dbo'


#check downloadOneURL
workingDir = os.getcwd() + "\\"
downloadFolderPath = workingDir + "stagging\\"
os.chdir(workingDir)

# Extract the NDVI tif file and wait 5 seconds after unpack
def uncompressNDVItifFile(zipFile, destinationFolder):
	#zipFile is the path to the .zip file
	#extract in the destinationFolder
	#returns the title name of the extracted NDVI tif file and status
	zifref = zipfile.ZipFile(zipFile,'r')
	fileList = zifref.namelist()
	for file in fileList:
		aList = file.split('.')
		if aList[4].split('_')[1] == 'NDVI' and aList[-1] == 'tif':
			name = '.'.join(aList)
			zifref.extract(name, destinationFolder)
			sleep(5)
			zifref.close()
			return [name,1]
	zifref.close()
	return ['NDVI tif file not found',0]

def dbf2csv(DBF, CSV):
	with open(CSV,'wb') as of:
		subprocess.check_call(["in2csv","--format","dbf", DBF], stdout = of)
	of.close()

def deleteAllfiles(destinationFolder):
	files = glob.glob(destinationFolder+'*')
	for f in files:
		try:
			os.remove(f)
		except:
			print "Couldn't remove the file: "+ f 

def calculateDate(Year, Day):
	D = datetime.date(int(Year),1,1) + datetime.timedelta(days = (int(Day)-1))
	return D

def dayTime(title):
	Year = int(title.split('.')[1])
	[StartDay, EndDay] = title.split('.')[2].split('-')
	if Year % 4 != 0:
		if int(StartDay) > 359:
			StartDate = calculateDate(Year-1, StartDay)
			EndDate = calculateDate(Year, EndDay)
		else:
			StartDate = calculateDate(Year, StartDay)
			EndDate = calculateDate(Year, EndDay)
	else:
		if int(StartDay) > 360:
			StartDate = calculateDate(Year-1, StartDay)
			EndDate = calculateDate(Year, EndDay)
		else:
			StartDate = calculateDate(Year, StartDay)
			EndDate = calculateDate(Year, EndDay)
	return [StartDate, EndDate]

#get the latest end date from the database
def query(db, schema, sqlServerInstance, workingDir,tableToWriteTo):
	connStr = "DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection = Yes"
	ed=0
	sd=0
	conn = pyodbc.connect(connStr)
	cursor = conn.cursor()
	query = "SELECT TOP 1 Start_Date, End_Date FROM ["+db+"].["+schema+"].["+tableToWriteTo+"] ORDER BY End_Date desc, Start_Date desc" ## enter the query to be executed here
	cursor.execute(query)
	for row in cursor:
		ed = row[1]
		sd = row[0]
	## close the connection once we get the values
	conn.close()
	return [sd, ed]


# gets us the latest "end date" from the URLs obtained from the website
# will be used while updating. This date will be compared with the current end date in DB
def currEndDate(URLS):
	L = len(URLS)
	for count,url in enumerate(URLS):
		if count == (L-1):
			fileName = url.split('/')[-1]
			ed = dayTime(fileName)[1]
	return ed

def FindURLSnotScraped(URLSfromweb, currDBendDate, currURLSendDate):
	#return a list of URLS that hasn't been scraped
	#it is achieved by comparing the last end date of the URLS and
	#the last end date in DB
	if currDBendDate < currURLSendDate:
		for count,url in enumerate(URLSfromweb):
			fileName =url.split("/")[-1]
			[sd,ed] = dayTime(fileName)
			#print sd,' ',ed
			if ed <= currDBendDate:
				#print sd,' ',ed,' ',count
				pass
			else:
				break
			#print count
		return URLSfromweb[count:]
	else:
		return []

## Functions for loading to DB##
def FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload,tableToWriteTo):
	conn = pyodbc.connect("DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection= Yes")
	cursor = conn.cursor()
	sqlcmd_one = """CREATE TABLE [dbo].["""+tableToWriteTo+"""]  (
	[Start_Date] date,
	[End_Date] date,
	[CDLYear] smallint,
	[Start_Month] smallint,
	[Start_Day] smallint,
	[Start_Week] smallint,
	[End_Month] smallint,
	[End_Day] smallint,
	[End_Week] smallint,
	[StateFIPS] smallint,
	[StateAbbrev] varchar(2),
	[CropName] varchar(9),
	[CDLCropCode] smallint,
	[FIPS] int,
	[count] smallint,
	[area] real,
	[min] smallint,
	[max] smallint,
	[range] smallint,
	[mean] real,
	[std] real,
	[sum] real,
	[variety] smallint,
	[majority] smallint,
	[minority] smallint,
	[median] smallint
	)"""
	sqlcmd_two ="""TRUNCATE TABLE [dbo].[""" +tableToWriteTo+ """]"""

	try:
		sqlcmd = sqlcmd_one
		cursor.execute(sqlcmd)
		cursor.commit()
		conn.close()
		print 'Initial create of ['+tableToWriteTo+'] Complete.'
	except Exception as e:
		sqlcmd = sqlcmd_two
		cursor.execute(sqlcmd)
		cursor.commit()
		conn.close()
	############ use bulk copy (bcp) to load the data into the CommodityFutures table
	theproc = subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + FileToUpload + ' -c -t,  -T -S' + sqlServerInstance)

def loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir, UpdateFileToUpload, tableToWriteTo):
	if os.path.isfile(workingDir + UpdateFileToUpload):
		theproc =subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + UpdateFileToUpload + ' -c -t,  -T -S' + sqlServerInstance)
		print 'New Data is uploaded'
	else:
		print "No New Data"




def setEnv(tiffFile):
	des = arcpy.Describe(tiffFile)
	arcpy.env.extent = des.extent
	arcpy.env.cellSize = des.children[0].meanCellHeight
	arcpy.env.outputCoordinateSystem = des.spatialReference
	arcpy.env.cartographicCoordinateSystem = des.spatialReference
	arcpy.env.pyramid = "PYRAMIDS -1 NEAREST 75 NO_SKIP"
	arcpy.env.randomGenerator = arcpy.CreateRandomValueGenerator(0, "ACM599")
	arcpy.derivedPrecision = "HIGHEST"
	arcpy.env.newPrecision = "SINGLE"
	arcpy.env.tileSize = "128 128"  ## default
	arcpy.env.tinSaveVersion = "CURRENT"
	arcpy.env.rasterStatistics = 'STATISTICS 1 1'
	arcpy.env.resamplingMethod = "NEAREST"
	arcpy.env.nodata = "NONE"
	arcpy.env.overwriteOutput = True
	arcpy.env.coincidentPoints="MAX"




#run the following codes to set up arcpy environment
arcpy.env.workspace = workingDir
arcpy.env.overwriteOutput=True
arcpy.CheckOutExtension("Spatial")

# Add 5 digit FIPS code to shape file
#add field for 5 digit fips code = stateFIPS*1000+countyFIPS. Note, already added but in original census files have to run the 2 lines below to get 5 digit fips
arcpy.AddField_management(in_table= workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp",field_name="FIPS",field_type="LONG")
arcpy.CalculateField_management(in_table= workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp",field="FIPS",expression="int( !STATEFP!)*1000+int( !COUNTYFP!)",expression_type="PYTHON_9.3")




#YEARS = range(2000, 2015)
def scrape(URLS,FileToWriteTo, workingDir):
	totalCount = 0 
	for loopCount,url in enumerate(URLS):
		print "begin loopCount: ", loopCount
		year = -1000
		stateFlag = 1 # boolean 0: need to download cdl, 1: don't
		
		""" Donwload the emodis file for a particular url"""
		fileName = url.split("/")[-1]
		print fileName
		[currEmodisStartDate, currEmodisEndDate] = dayTime(fileName)
		currYear = currEmodisStartDate.year  
		print "current Year......", currYear 
		print 
		try:
			downloadOneURL_emodis.main(workingDir, url)
			print "downloaded properly"
			sleep(5)
		except: 
			print "Failed to download"
			print "Retrying once more"
			downloadOneURL_emodis.main(workingDir, url)
			sleep(5)
			continue # continue to the next url
		#unzip 
		#zipFilePath = downloadFolderPath+fileName
		zipFile = workingDir + "staging\\" + fileName
		print zipFile
		try:
			[emodisFileName, status] = uncompressNDVItifFile(zipFile, workingDir+"staging")
			print "emodisFileName ", emodisFileName
			print "Uncompressing Done"
			print "status: ", status
			sleep(5)
		except:
			print "Not a zip file, setting status to 0"
			#loopCount += 1
			status = 0
		
		if year != currYear:
			if loopCount != 0:
				# in this case, we should delete the cdl files from the previous year
				deleteAllfiles(workingDir+"cdl_staging\\")
				stateFlag = 0
			else:
				year = currYear
				stateFlag = 0

		if status == 1:
			print "To take care of Nodata, we need to use SetNull_sa method;"
			print "\n"
			ndvi_tiff_file = workingDir + "staging\\" + emodisFileName
			print ndvi_tiff_file
			print "\n"
			arcpy.gp.SetNull_sa(ndvi_tiff_file, ndvi_tiff_file, workingDir+"staging\\"+"newTiff.tif" , """"Value" =-2000""")
			print "conversion done......"
			new_ndvi_tiff_file = workingDir+"staging\\" + "newTiff.tif"
			print new_ndvi_tiff_file
			print "\n"
			shapeFile = workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp"
			dbfFile = workingDir + '_'.join(emodisFileName.split('.')[:-1])+'.dbf'
			csvFile = workingDir +  '_'.join(emodisFileName.split('.')[:-1])+'.csv'
			print "\n"
			print dbfFile
			print "\n"
			print csvFile
			print "\n"
			print "original NDVI file is no longer needed. Removing it....."
			print "\n"
			try:
				os.remove(ndvi_tiff_file)
				print ndvi_tiff_file, " removed"
				print "\n" 
			except:
				print "Couldn't remvoe the file as some process is uing it"
				print "\n"
			#set environment variables
			setEnv(new_ndvi_tiff_file)
			for state in stateFIPS.keys():
				#currState = None
				print 'state: ', state
				print "\n"
				if year != currYear or stateFlag == 0:
					"""Download the CDL file for the state and year"""
					try:
						cdlName = downloadOneURL_cdl.main(workingDir, state, currYear) #works correctly
						#cdlName = "CDL_2000_17.tif"
						print len(cdlName)
						year = currYear
						#raw_input("Enter to continue......")
						if len(cdlName) == 0:
							continue
					except: 
						continue
				cdl_tiff_file_name = workingDir+"cdl_staging\\"+cdlName
				for crop in cropCodes.keys():
					print "crop:......", crop 
					dbfFile = workingDir + '_'.join(emodisFileName.split('.')[:-1])+'_%s_%s' % (state,crop)+'.dbf'
					csvFile = workingDir +  '_'.join(emodisFileName.split('.')[:-1])+'_%s_%s' % (state,crop)+'.csv'
					print "\n"
					print dbfFile
					print "\n"
					print csvFile
					print "\n"
					output_con_raster = workingDir+"staging\\"+ "con_%s_%s" %(state,crop)+"_"+cdlName
					print output_con_raster
					print 
					try:
						#arcpy.gp.Con_sa(workingDir+"staging\\"+cdlName, new_ndvi_tiff_file, output_con_raster, """"Value" = %d""" % cropCodes[crop])
						arcpy.gp.Con_sa(cdl_tiff_file_name, new_ndvi_tiff_file, output_con_raster,"", """"Value" = %d""" % cropCodes[crop])
						print "conditional raster done......"
						print 
					except:
						print "conditional raster failed......"
						continue 
					
					print "start calculation of zonal statistics..."
					print
					try:
						arcpy.gp.ZonalStatisticsAsTable_sa(shapeFile, "FIPS", output_con_raster, dbfFile, "DATA", "ALL")
						print "calculation done....."
						print
					except :
						print "Zonal stat calculation failed........."
						continue 

					print "convertion of dbf to csv is starting now........"
					print "\n"
					try:
						dbf2csv(dbfFile, csvFile)
						print "conversion complete.........."
					except:
						print "conversion to csvFile failed......"
						print "deleting it......"
						try:
							os.remove(dbfFile)
						except:
							print "dbfFile couldn't be removed......"
						print "continue to the next crop......"
						continue 

					print "read in the csvFile created above ............"
					tempDF = pd.read_csv(csvFile)
					print tempDF.head()
					print
					"""If there is no crop dbfFile will be empty, and hence correspnding csvFile will be empty
					In that case tempDF will have index 0. we should go to the next crop if that's there or go to the next state or to next ndvi file"""
					if tempDF.shape[0] == 0:
						print "there is nothing in the dbfFile/csvFile so tempDF is empty...."
						print "\n"
						print "we are cycle through to the next crop or if no crop is there ...go to the next state....."
						print "\n"
						try:
							os.remove(csvFile)
						except:
							print "cannot remove the file as it is being used by another process......"
						try:
							os.remove(dbfFile)
						except:
							print "cannot remove the file as it is being used by another process......"
						try:
							os.remove(dbfFile.split('.')[0]+'.cpg')
						except:
							print "cannot remove the file as it is being used by another process......"
						try:
							os.remove(dbfFile.split('.')[0]+'.dbf.xml')
						except:
							print "cannot remove the file as it is being used by another process......"
						try:
							os.remove(output_con_raster)
						except:
							print "cannot remove the file as it is being used by another process......"
						continue
					print "some rows are from adjacent states....droping those below......"
					mask = tempDF.fips.apply(lambda x: False if int(np.floor(x/1000)) == int(stateFIPS[state]) else True)
					tempDF =  tempDF.drop(tempDF.index[mask])
					tempDFcolNames = tempDF.columns.tolist()
					tempStartDate = currEmodisStartDate.strftime('%Y-%m-%d')
					tempEndDate = currEmodisEndDate.strftime('%Y-%m-%d')
					tempStartMonth = currEmodisStartDate.month
					tempStartDay = currEmodisStartDate.day
					tempStartWeek = currEmodisStartDate.isocalendar()[1]
					tempEndMonth = currEmodisEndDate.month
					tempEndDay = currEmodisEndDate.day
					tempEndWeek = currEmodisEndDate.isocalendar()[1]
					tempDF['Start_Date'] = [tempStartDate] * (tempDF.shape[0])
					tempDF['End_Date'] = [tempEndDate] * (tempDF.shape[0])
					tempDF['CDLYear'] = [currYear] * (tempDF.shape[0])
					tempDF['Start_Month'] = [tempStartMonth] * (tempDF.shape[0])
					tempDF['Start_Day'] = [tempStartDay] * (tempDF.shape[0])
					tempDF['Start_Week'] = [tempStartWeek] * (tempDF.shape[0])
					tempDF['End_Month'] = [tempEndMonth] * (tempDF.shape[0])
					tempDF['End_Day'] = [tempEndDay] * (tempDF.shape[0])
					tempDF['End_Week'] = [tempEndWeek] * (tempDF.shape[0])
					tempDF['StateFIPS'] = [stateFIPS[state]] * (tempDF.shape[0])
					tempDF['StateAbbrev'] = [state] * (tempDF.shape[0])
					tempDF['CropName'] = [crop] * (tempDF.shape[0])
					tempDF['CDLCropCode'] = [cropCodes[crop]] * (tempDF.shape[0])
					tempDF = tempDF[['Start_Date','End_Date','CDLYear', 'Start_Month', 'Start_Day', 'Start_Week', 'End_Month', 'End_Day', 'End_Week', \
					'StateFIPS','StateAbbrev','CropName','CDLCropCode']+tempDFcolNames]
					print "Now changing the column name of fips to FIPS.........."
					tempDFcolNames = tempDF.columns.tolist()
					tempDFcolNames[tempDFcolNames.index('fips')] = 'FIPS'
					tempDF.columns = tempDFcolNames
					#raw_input("Enter here to proceed......")
					print tempDF.head()
					print "temporary DF is ready.....................write to file......."
					#may need to rearrange the columns before writing back 
					if totalCount == 0:
						tempDF.to_csv(FileToWriteTo, index = False)
						totalCount += 1
						print "Data written to....." + FileToWriteTo
						print
					else:
						with open(FileToWriteTo, 'a') as f:
							tempDF.to_csv(f, index = False, header = False)
							f.close()
							totalCount +=1
							print "Data written to....." + FileToWriteTo
							print

					print "remove the temporary csvFile ........" 
					try:
						os.remove(csvFile)
						print "csvFile removed......."
					except:
						print "Couldn't remove the file......"

					print "remove the dbf and associated files"
					try:
						os.remove(dbfFile)
						print "dbf file removed........."
					except:
						print "dbf file couldn't be removed........"

					try:
						os.remove(dbfFile.split('.')[0]+'.cpg')
					except:
						print "Couldn't remove the .cpg file........"
						print "\n"
					try:
						os.remove(dbfFile.split('.')[0]+'.dbf.xml')
					except:
						print "Couldn't remvoe the .dfb.xml file.........."
						print "\n"
					print "don't need ......."+ output_con_raster +"removing it now......."
					print "\n"
					
					try:
						os.remove(output_con_raster)
						print output_con_raster + "......removed......."
						print "\n"
					except:
						print "output_con_raster couldn't be reomvoed...."
						print "\n"
			print "all states are done......."
			print "\n"
			print "clean up staging folder......."
			print "\n"
			deleteAllfiles(workingDir + "staging\\")
			print "End of loopCount: ", loopCount
		else:
			print "status is 0"
			print "End of loopCount: ", loopCount
	if loopCount == (len(URLS) - 1):
		print "clearing out the cdl_staging area as this is the last loop"
		deleteAllfiles(workingDir +"cdl_staging\\")
		print "process complete"



def main(argv):
	if not argv or len(argv) != 1:
		print "Wrong Command: Enter 'I' or 'i' for initial scraping and 'U' or 'u' for update"
		sys.exit(1)

	flag = argv[0]
	if flag == 'I' or flag == 'i':
		print "Getting URLS from the eMODIS website to download the NDVI tiff files"
		#URLS = getAllURLS.main()
		"""uncomment the below lines when running on a debug mode and comment out the above line"""
		with open('URLS.txt', 'r') as handle:
			URLS = pickle.load(handle)
		handle.close()
		print "URLS downloaded ", len(URLS)
		print "\n"
		print 'Begin Scraping'
		print "\n"
		scrape(URLS[:1],FileToWriteTo, workingDir)
		print "Historical data download procedure completes..........."
		print "\n"
		print "Begin Upload to DB.........."
		FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload, tableToWriteTo)
		print "Data Uploaded to DB.........."
	elif flag == 'U' or flag == 'u':
		"""Need all the URLS once again"""
		#URLS = getAllURLS.main()
		"""uncomment the below lines when running on a debug mode and comment out the above line"""
		with open('URLS.txt', 'r') as handle:
			URLS = pickle.load(handle)
		handle.close()
		currED = currEndDate(URLS)
		print "last date available is "+ str(currED)
		[currDBstartDate, currDBendDate] = query(db, schema, sqlServerInstance, workingDir,tableToWriteTo)
		currDBendDate = datetime.datetime.strptime(currDBendDate, '%Y-%m-%d').date()
		newURLS = FindURLSnotScraped(URLS, currDBendDate, currED)
		print "last end date available in DB is..."+ str(currDBendDate.strftime('%A %d, %B,%Y ')).strip()
		if len(newURLS) > 0:
			print "New files are available......"
			"""change the newURLS once debugging is done"""
			scrape(newURLS[:1], FileToWriteToUpdate, workingDir)
			print "Data donwloaded to the file...." + FileToWriteToUpdate
			print "\n"
			print "Uploading new data to DB..........."
			loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir, UpdateFileToUpload, tableToWriteTo)
		else:
			print "There is no new data available at this time............"


if __name__ == "__main__":
	main(sys.argv[1:])