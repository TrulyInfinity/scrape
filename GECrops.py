
# coding: utf-8

# In[1]:

import pandas as pd
import os,sys
import urllib
import pyodbc
import subprocess


# In[204]:

## set the global variables here
#workingDir = "E:\\DatabaseFiles\\UpdateDBFiles\\EthanolPlants\\"
workingDir = os.getcwd()+"\\"
#sqlServerInstance = 'AG-AEM-1M9RBY1'
#sqlServerInstance = ".\mssqlsvrag"
sqlServerInstance = ".\MSDEV"
schema = 'dbo'
db = 'AgDB'


FileToUpload = 'GECrops.csv'
tableToWriteTo = 'GECrops'


# In[173]:

def isNumber(x):
    try:
        float(x)
        return True
    except:
        return False

   


# In[175]:

def convertToNull(x):
    if not isNumber(x):
        return None
    else:
        return x
    
   


# In[2]:

url = 'http://www.ers.usda.gov/dataFiles/Adoption_of_Genetically_Engineered_Crops_in_the_US/alltablesGEcrops.csv'
testFile = urllib.URLopener()
testFile.retrieve(url,'alltablesGEcrops.csv')


# In[4]:

df = pd.read_csv('alltablesGEcrops.csv')


# In[127]:

#df.ix[df.State == 'Other States',:]


# In[19]:

FIPScode = pd.read_csv('FIPS_code_state.csv')


# In[23]:

#df.State.unique()


# In[115]:

#clean up the state names
df.State = df.State.str.replace(' 2/','').str.replace(' 1/','').str.rstrip()
UniqueStateNames = df.State.str.replace(' 2/','').str.replace(' 1/','').str.rstrip().unique()


# In[116]:

UniqueStateNames


# In[207]:

StateFipsCode = dict()
for name in UniqueStateNames:
    if name not in ['Other States', 'U.S.']:
         StateFipsCode[name] = FIPScode.ix[(FIPScode['StateName'] == name),[2,3]].StateFIPS.tolist()[0]
    elif name == 'U.S.':
        StateFipsCode[name] = 99#MAY NEED TO CHANGE THIS 
    else:
        StateFipsCode[name] = 98


# In[188]:

StateAbrvCode = dict()
for name in UniqueStateNames:
    if name not in ['Other States', 'U.S.']:
        StateAbrvCode[name] = FIPScode.ix[(FIPScode['StateName'] == name),[2,19]].StateAbbrev.tolist()[0]
    else:
        StateAbrvCode[name] = name


# In[208]:

print StateAbrvCode
print
print StateFipsCode


# In[214]:

#get the State FIPS code ( because our data is only state based:change if needed)
StateFips = [0]*len(df)
for i,name in enumerate(df.State.tolist()):
    StateFips[i] = StateFipsCode[name]
 
    


# In[190]:

#get the State FIPS code ( because our data is only state based:change if needed)
StateAbrv = [0]*len(df)
for i,name in enumerate(df.State.tolist()):
    StateAbrv[i] = StateAbrvCode[name]


# In[193]:

df['StateAbbrv'] = StateAbrv


# In[216]:

#add a column to the dataframe with StateFIPS
#df['StateFIPS'] = a
df['StateFIPS'] = StateFips
df.StateFIPS = StateFips #correction made to StateFIPS


# In[226]:

#df.ix[df.StateFIPS==99,:]


# In[219]:

newdf = df[['Crop', 'State','StateAbbrv','StateFIPS','Year','Variety','Unit','Value']]


# In[221]:

newdf.columns = ['Crop', 'State','StateAbbrv','StateFIPS','Year','Type','Unit','Value']
newdf.head()


# In[222]:

#need to strip off white spaces at the end of Unit and Type variables
newdf.Type = newdf.Type.str.rstrip()

newdf.Unit = newdf.Unit.str.rstrip()


# In[223]:

#need to remove non-numbers from the Value column
newdf.Value = newdf.Value.apply(convertToNull)


# In[224]:

newdf.to_csv('GECrops.csv', index = False)


# In[203]:

def FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload,tableToWriteTo ):
    conn = pyodbc.connect("DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection= Yes")
    cursor = conn.cursor()
    sqlcmd_one = """CREATE TABLE [dbo].["""+tableToWriteTo+"""]  (
    [Crop] varchar(39),
    [State] varchar(42),
    [StateAbbrv] varchar(36),
    [StateFIPS] bigint,
    [Year] bigint,
    [Type] varchar(78),
    [Unit] varchar(96),
    [Value] bigint
    )"""
    sqlcmd_two ="""TRUNCATE TABLE [dbo].["""+tableToWriteTo+"""]"""

    try:
        sqlcmd = sqlcmd_one
        cursor.execute(sqlcmd)
        cursor.commit()
        conn.close()
        print 'Initial create of [' + tableToWriteTo +'] Complete.'
    except Exception as e:
        sqlcmd = sqlcmd_two
        cursor.execute(sqlcmd)
        cursor.commit()
        conn.close()
    ############ use bulk copy (bcp) to load the data into the CommodityFutures table
    print "About to upload"
    theproc = subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + FileToUpload + ' -c -t,  -T -S' + sqlServerInstance)
    print "Uploaded"


# In[225]:

#Main#
FlatFileToUpload(db, schema, sqlServerInstance, workingDir,FileToUpload,tableToWriteTo)


# In[158]:

#workingDir+'GECrops.csv'

