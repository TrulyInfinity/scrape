import os
import sys
import arcpy
import datetime
import UtilNDVI
#import getAllURLS
#import downloadOneURL



#global variables
FileToWriteTo = 'eMODIS.csv'
FileToWriteToUpdate = 'eMODIS_update.csv'
tableToWriteTo = 'eMODIS'
FileToUpload = FileToWriteTO
UpdateFileToUpload = FileToWriteToUpdate

#global variables for SQL server instance
sqlServerInstance = '.\MSDEV' #check
db = 'AgDB'
schema = 'dbo'


#set the working directory and other paths
workingDir = os.getcwd() + "\\"
print workingDir
os.chdir(workingDir)

#the downloaded files will be in a temporary folder called 'staging'
downloadZipFileToPath = workingDir + "staging\\"
if not os.path.exists(downloadZipFileToPath):
        os.makedirs(downloadZipFileToPath)


#run the following codes to set up arcpy environment
arcpy.env.workspace = workingDir
arcpy.env.overwriteOutput=True
arcpy.CheckOutExtension("Spatial")

# Add 5 digit FIPS code to shape file
#add field for 5 digit fips code = stateFIPS*1000+countyFIPS
arcpy.AddField_management(in_table= workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp",field_name="FIPS",field_type="LONG")
arcpy.CalculateField_management(in_table= workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp",field="FIPS",expression="int( !STATEFP!)*1000+int( !COUNTYFP!)",expression_type="PYTHON_9.3")


def main(argv):
    if not argv or len(argv) !=1:
        print 'Wrong Command: Enter I or i for initial load or U or u for update'
        sys.exit(1)

    flag = argv[0]
    if flag == 'I' or flag == 'i':
        print "Getting URLS from the website"
        URLS = UtilNDVI.getAllurls()
        print 'URLS downloaded ', len(URLS)
        print 'Begin Scraping'
        UtilNDVI.scrape(URLS, flag, FileToWriteTo, workingDir)
        print 'Scraping Historical Data Done'
        print 'Begin uploading to the DB'
        #upload the file to the database
        UtilNDVI.FlatFileToUpload(db, schema, sqlServerInstance, workingDir,FileToUpload,tableToWriteTo)
        print 'Historical Data Uploaded'
    elif flag == U or flag == u:
        URLS = UtilNDVI.getAllurls()
        L = len(URLS)
        currED = UtilNDVI.currentEndDate(URLS) # website
        [currStartDate, currDBendDate] = UtilNDVI.query(db, schema, sqlServerInstance, workingDir,tableToWriteTo) # our database
        currDBendDate = datetime.datetime.strptime(currDBendDate,'%Y-%m-%d').date()
        newURLS = UtilNDVI.FindURLSnotScraped(URLS, currDBendDate, currED)
        if len(newURLS) > 0:
            #somethign to scrape
            UtilNDVI.scrape(newURLS, flag, FileToWriteToUpdate, workingDir)
            UtilNDVI.loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir,UpdateFileToUpload, tableToWriteTo)
        else:
            print 'No new data to upload'



if __name__ == "__main__":
    main(sys.argv[1:])