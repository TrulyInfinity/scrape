### Note

* This contains geospatial data collected from satellite images (tiff file) containing reflective indices from earth surfaces. These aggregated based on U.S.
counties

* This program makes heavy use of arcpy module available from ArcGIS

* Two associated python files (gitALLURLS and downloadONEurl) are not included as it contains sensitive information. They basically scrape all the historical linkes to the data and then download one at a time.