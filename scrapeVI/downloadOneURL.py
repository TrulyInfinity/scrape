# This is script is called by main.py and UtilNDVI.py and downloads the given NDVI file from USGS FTP site.
# User should update with their USGS user and password

import logging
import os
import sys
import zipfile
import requests
import shutil
import popen2
import requests


def main(workingDir,URL):

    print "begin downloading FROM" + URL
    logging.info("begin downloading FROM" + URL)

    req = requests.get(URL, auth=('# Ag-Analytics.Org eMODIS NDVI US Counties Download ETL program (1 of 4 files)
# Copyright Joshua D. Woodard, 2016
# See License at: https://www.ag-analytics.org/AgRiskManagement/Documents/Terms_Copyright.pdf
# Thanks to students Hanwen Wang, Haowen Tao, and Tridib Ditta for helping with parts of the script.

# This is script is called by main.py and ScrapingEmodis.py and downloads the given NDVI file from USGS FTP site.
# User should update with their USGS user and password

import logging
import os
import sys
import zipfile
import requests
import shutil
import popen2
import requests


def main(workingDir,URL):

    print "begin downloading FROM" + URL
    logging.info("begin downloading FROM" + URL)

    req = requests.get(URL, auth=('****', '****'),stream = True)

    #Download file to staging folder, create folder if it does not already exist
    downLoadPath = workingDir + "staging\\"
    if not os.path.exists(downLoadPath):
        os.makedirs(downLoadPath)

    #Get the name of the file (note, there is a random (published on?) date at end of file which can not be ascertained ahead of time, so have to scrape it
    logging.info("begin get the name of downloaded File" + URL)
    URLCompList = URL.split('/')
    downLoadFileName = URLCompList[-1]
    title = downLoadFileName[:-4]
    logging.info("finish get the name of downloaded File" + URL)

    #Copy downloaded file to staging folder
    logging.info("begin downloading" + title)
    with open(downLoadPath + downLoadFileName, 'wb') as fp:
        for chunk in req.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                fp.write(chunk)
                fp.flush()
    fp.close()
    print "finish downloading"
    logging.info("finish downloading" + title)
if __name__ == "__main__":
    main()
', 'USGSwoodard14'),stream = True)

    #Download file to staging folder, create folder if it does not already exist
    downLoadPath = workingDir + "staging\\"
    if not os.path.exists(downLoadPath):
        os.makedirs(downLoadPath)

    #Get the name of the file (note, there is a random (published on?) date at end of file which can not be ascertained ahead of time, so have to scrape it
    logging.info("begin get the name of downloaded File" + URL)
    URLCompList = URL.split('/')
    downLoadFileName = URLCompList[-1]
    title = downLoadFileName[:-4]
    logging.info("finish get the name of downloaded File" + URL)

    #Copy downloaded file to staging folder
    logging.info("begin downloading" + title)
    with open(downLoadPath + downLoadFileName, 'wb') as fp:
        for chunk in req.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                fp.write(chunk)
                fp.flush()
    fp.close()
    print "finish downloading"
    logging.info("finish downloading" + title)
if __name__ == "__main__":
    main()
