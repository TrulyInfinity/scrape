
# This is script is called by main.py and generates a list of all URLS for NDVI files to download from USGS FTP site.
# User should update with their USGS user and password

#now this file can get all URL list, so this is specific to every task
import urllib2
import shutil
import os
import popen2
import requests
import pickle
from lxml import html

def main():
    print "getting URLS..."
    url = 'https://dds.cr.usgs.gov/emodis/CONUS/historical/TERRA/'
    r = requests.get(url, auth=('*****', '*****'))
    print "initial login successful to "
    print r
    tree = html.fromstring(r.text)
    aList = tree.xpath('//a/text()')                                            #alist is the tree structure which contains all years

    URLList = []

    for year in aList:
        print year
        year = year.strip()
        print year
        subURL = url + year
        #print subURL
        try:
            subRequest = requests.get(subURL, auth=('*****', '*****'))
        except Exception:
            print "failed to get server connection, trying again for folders"
            print subURL
            subRequest = requests.get(subURL, auth=('*****', '*****'))
            
            
        subtree = html.fromstring(subRequest.text)
        subaList = subtree.xpath('//a/text()')
        #print subaList
        for day in subaList:
            #this is to get rid of the first and end whitespace
            day = day.strip()
            if(day[-2] != 'y'):                                                 #I'm not sure wheather nor not we need the 14-day shape, just get the data on 7-day basis
                downloadURL = subURL + day
                #print downloadURL
                URLList.append(downloadURL)
    #print URLList
    #this is to return the list from one file

    URLZiplist = [] #URLZiplist is the specific download URL list. all linkas are stored in URL form
    print "Getting files URLs"
    for url in URLList:
        print "Getting file name in URL " +url
        
        try:
            #print "getting file name urls in "
            #print url
            r = requests.get(url, auth=('*****', '*****'))
            tree = html.fromstring(r.text)
            aList = tree.xpath('//a/text()')                                        #for every link, get the fifth link which is in this format US_eMTH_NDVI.2015.293-299.QKM.COMPRES.005.2015303011712.zip\
        except Exception:
            print "failed to get server connection, trying again for files in"
            print url
            r = requests.get(url, auth=('*****', '*****'))
            tree = html.fromstring(r.text)
            aList = tree.xpath('//a/text()')                                        #for every link, get the fifth link which is in this format US_eMTH_NDVI.2015.293-299.QKM.COMPRES.005.2015303011712.zip\
            print "got on 2nd try"
        #download the 5th URLfor example: US_eMTH_NDVI.2015.293-299.QKM.COMPRES.005.2015303011712.zip\
        try:
            #tem = aList[4].strip() # changed to find the NDVI QKM file specifically
            for ff in aList:
                ff_list = ff.split(".")
                if ff_list[-1].strip() == "zip" and ff_list[3] == "QKM" and ff_list[0].split("_")[2] == "NDVI":
                    print "found file"
                    tem = ff
                    specificUrl = url.strip() + tem.strip()
                    URLZiplist.append(specificUrl)
                    f = open('URLS.txt', 'w')
                    pickle.dump(URLZiplist, f)
                    f.close()
                    break

            
            
            #print "got this specific url"
            #print specificUrl
        except Exception as e:
            print(e)
            print "there is no such file, going to next folder"
            #this happens when for example there is folder called: https://dds.cr.usgs.gov/emodis/CONUS/historical/TERRA/2016/substitute/
            #in this case there is not enough files.
            #the program always grabs 5th file, which could be problematic, maybe in fturue we could fix jdw8-12-2016

            
            
    print "success! List is in URLZiplist"
    subRequest.connection.close() 
    r.connection.close() 
    #print URLZiplist
    return URLZiplist
if __name__ == "__main__":
    main()