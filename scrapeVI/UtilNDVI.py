#### Author: TRIDIB DUTTA
#### Program scrapes reflectance index data (vegitation index) from satellite imagary (Tiff files)

#Modules needed
import pandas as pd
import os
import sys
import urllib
import urllib2
import subprocess
import csvkit
import arcpy
from arcpy import env
import pyodbc
import datetime
import glob
from time import sleep
import zipfile

#local modules we need
import getAllURLS #get all the urls
import downloadOneURL # download one file from one of the URL's




#set the working directory and other paths
workingDir = os.getcwd() + "\\"
print workingDir

#the downloaded files will be in a temporary folder called 'staging'
downloadZipFileToPath = workingDir + "staging\\"
if not os.path.exists(downloadZipFileToPath):
        os.makedirs(downloadZipFileToPath)



#get all the URLs
def getAllurls():
    URLS = getAllURLS.main()
    #print len(URLS)
    return URLS



#calculate the date from the Year and Julian Day
def calculateDate(Year, Day):
    D = datetime.date(int(Year),1,1) + datetime.timedelta(days = (int(Day) - 1) )
    return D

#calculates the Year, Month, Start date, end date etc from the title
def dayTime(title):
    Year = int(title.split('.')[1])
    [StartDay, EndDay] = title.split('.')[2].split('-')
    if Year % 4 !=0:
        if int(StartDay) > 359:
            StartDate = calculateDate(Year-1, StartDay)
            EndDate = calculateDate(Year,EndDay)
        else:
            StartDate = calculateDate(Year, StartDay)
            EndDate = calculateDate(Year,EndDay)
    else:
        if int(StartDay) > 360:
            StartDate = calculateDate(Year-1, StartDay)
            EndDate = calculateDate(Year,EndDay)
        else:
            StartDate = calculateDate(Year, StartDay)
            EndDate = calculateDate(Year,EndDay)

    return [StartDate, EndDate]


# gets us the latest "end date" from the URLs obtained from the website
# will be used while updating. This date will be compared with the current end date in DB
def currEndDate(URLS):
    L = len(URLS)
    for count,url in enumerate(URLS):
        if count == (L-1):
            fileName = url.split('/')[-1]
            ed = dayTime(fileName)[1]
    return ed


#get the latest end date from the database
#tableToWriteTo = 'eMODIS'

def query(db, schema, sqlServerInstance, workingDir,tableToWriteTo):
    connStr = "DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection = Yes"
    ed=0
    sd=0
    conn = pyodbc.connect(connStr)
    cursor = conn.cursor()
    query = "SELECT TOP 1 Start_Date, End_Date FROM ["+db+"].["+schema+"].["+tableToWriteTo+"] ORDER BY End_Date desc, Start_Date desc" ## enter the query to be executed here
    cursor.execute(query)
    for row in cursor:
        ed = row[1]
        sd = row[0]

    ## close the connection once we get the values
    conn.close()
    return [sd, ed]


# Following function extract only the NDVI....tif file
def uncompressNDVItifFile(zipFile, destinationFolder):
    #zipFile is the path to the .zip file
    #extract in the destinationFolder
    #returns the title name of the extracted NDVI tif file and status
    zifref = zipfile.ZipFile(zipFile,'r')
    fileList = zifref.namelist()
    for file in fileList:
        aList = file.split('.')
        if aList[4].split('_')[1] == 'NDVI' and aList[-1] == 'tif':
            name = '.'.join(aList)
            zifref.extract(name, destinationFolder)
            sleep(5)
            zifref.close()
            return [name,1]
    zifref.close()
    return ['NDVI tif file not found',0]


#upload
#FileToUpload = FileToWriteTo
#UpdateFileToUpload = FileToWriteToUpdate

def FindURLSnotScraped(URLSfromWeb, currDBendDate, currURLSendDate):
    #return a list of URLS that hasn't been scraped
    #it is achieved by comparing the last end date of the URLS and
    #the last end date in DB
    if currDBendDate < currURLSendDate:
        for count,url in enumerate(URLSfromWeb):
            fileName =url.split("/")[-1]
            [sd,ed] = dayTime(fileName)
            #print sd,' ',ed
            if ed <= currDBendDate:
                #print sd,' ',ed,' ',count
                pass
            else:
                break
        #print count
        return URLS[count:]
    else:
        return []


## Loading Functions##
def FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload,tableToWriteTo ):
    conn = pyodbc.connect("DRIVER={SQL Server};SERVER="+sqlServerInstance+";DATABASE="+db+";Trusted connection= Yes")
    cursor = conn.cursor()
    sqlcmd_one = """CREATE TABLE [dbo].["""+tableToWriteTo+"""]  (
    [Start_Date] date,
    [End_Date] date,
    [fips] bigint,
    [mean] float
    )"""
    sqlcmd_two ="""TRUNCATE TABLE [dbo].[""" +tableToWriteTo+ """]"""

    try:
        sqlcmd = sqlcmd_one
        cursor.execute(sqlcmd)
        cursor.commit()
        conn.close()
        print 'Initial create of ['+tableToWriteTo+'] Complete.'
    except Exception as e:
        sqlcmd = sqlcmd_two
        cursor.execute(sqlcmd)
        cursor.commit()
        conn.close()
    ############ use bulk copy (bcp) to load the data into the CommodityFutures table
    theproc = subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + FileToUpload + ' -c -t,  -T -S' + sqlServerInstance)


def loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir, UpdateFileToUpload, tableToWriteTo):
    if os.path.isfile(workingDir + UpdateFileToUpload):
        subprocess.call('bcp '+db+'.'+schema+'.'+ tableToWriteTo + ' in ' + workingDir + UpdateFileToUpload + ' -c -t,  -T -S' + sqlServerInstance)
        print 'New Data is uploaded'
    else:
        print "No New Data"



#since des, extent, cellSize, outputCoordinateSystem and cartographicCoordinateSystem depends
#on emodisfile,a.k.a the tiff file in question, we need to set it for each tif file(I guess)

def setEnv(tiffFile):
    des = arcpy.Describe(tiffFile)
    arcpy.env.extent = des.extent
    arcpy.env.cellSize = des.children[0].meanCellHeight
    arcpy.env.outputCoordinateSystem = des.spatialReference
    arcpy.env.cartographicCoordinateSystem = des.spatialReference
    arcpy.env.pyramid = "PYRAMIDS -1 NEAREST LZ77 75 NO_SKIP"
    arcpy.env.randomGenerator = arcpy.CreateRandomValueGenerator(0, "ACM599")
    arcpy.derivedPrecision = "HIGHEST"
    arcpy.env.newPrecision = "SINGLE"
    arcpy.env.tileSize = "128 128"  ## default
    arcpy.env.tinSaveVersion = "CURRENT"
    arcpy.env.rasterStatistics = 'STATISTICS 1 1'
    arcpy.env.resamplingMethod = "NEAREST"
    arcpy.env.nodata = "NONE"
    arcpy.env.overwriteOutput = True
    arcpy.env.coincidentPoints="MAX"




#This function takes a list of URLs and srape data from it
#flag is a indicator obtain from the main() which indicate if U/u or I/i
def scrape(URLS, flag, FileToWriteTO, workingDir):
    count = 0
    while count < len(URLS):  #for count,url in enumerate(URLS):
        loopCount = count
        url = URLS[count]
        fileName= url.split("/")[-1]
        print "zipfile ", fileName
        #step0. Set up so that don't have to enter password and username manually
        downloadOneURL.main(workingDir, url)
        #step0a. unzip the .zip folder using subprocess
        print "Begin uncompressing......"
        #subprocess.call(workingDir + '7z.exe e -o' + downloadZipFileToPath + ' ' + downloadZipFileToPath + fileName + " -aoa")
        zipFilePath = downloadZipFileToPath+fileName
        [title,status] = uncompressNDVItifFile(zipFilePath, downloadZipFileToPath)
        #sleep(5)
        print "Uncompressing Done....."
        #get the name of the correct file
        #title = getNDVItifFileName() # it goes into the staging area and retrieves the appropriate tiff file name
        if status == 1:
            print "title of the tiff file ", title
            #step4.
            tiffFile = downloadZipFileToPath + title
            #print tiffFile
            shapeFile = workingDir + "cb_2014_us_county_500k\\cb_2014_us_county_500k.shp"

            dbfFile = workingDir + '_'.join(title.split('.')[:-1])+'.dbf'

            csvFile = workingDir + '_'.join(title.split('.')[:-1])+'.csv'

            #To take care of Nodata, we need to use SetNull_sa method;
            #SetNull_sa method generates a new .tif file
            print "converting -2000 to NoData........"
            arcpy.gp.SetNull_sa(tiffFile, tiffFile, downloadZipFileToPath+"newTiff.tif" , """"Value" =-2000""")
            print "conversion done........."

            #remove the old tiffFile
            try:
                os.remove(tiffFile)
            except:
                print 'Could not remove the file as some process is using it'
            #create a new tiffFile
            newtiffFile = downloadZipFileToPath+"newTiff.tif"
            #set enviroment variables
            setEnv(newtiffFile)

            print "start calculation of zonal statistics..."
            arcpy.gp.ZonalStatisticsAsTable_sa(shapeFile, "FIPS", newtiffFile, dbfFile, "DATA", "MEAN")
            print "calculation done....."

            print "convertion of dbf to csv starting now......"
            subprocess.call(["in2csv","--format","dbf",dbfFile,">",csvFile],shell=True)
            print "convertion complete....."
            print
            print "Removing some of the unnecessary files"
            #step6.
            try:
                os.remove(dbfFile)
            except:
                print 'Could not remove the file as some process is using it'
            #print dbfFile," file is now removed from directory"
            try:
                os.remove(dbfFile.split('.')[0]+'.cpg')
            except:
                print 'Could not remove the file as some process is using it'
            #print ".cpg file is now removed from directory"
            try:
                os.remove(dbfFile.split('.')[0]+'.dbf.xml')
            except:
                print 'Could not remove the file as some process is using it'
            #print ".dbf.xml file is now removed from directory"
            print "read in the .csv into DF for restructuring"
            #step7a.
            tempDF = pd.read_csv(csvFile)
            #step7b.
            [a, b] = dayTime(title)
            tempStartDate = a.strftime('%Y-%m-%d')
            tempEndDate = b.strftime('%Y-%m-%d')
            SDate = [tempStartDate]*(tempDF.shape[0])
            EDate = [tempEndDate]*(tempDF.shape[0])
            #step7c.
            tempDF = tempDF[['fips','mean']]
            #temp7d.
            tempDF['Start_Date'] = SDate
            tempDF['End_Date']  = EDate
            tempDF = tempDF[['Start_Date','End_Date','fips','mean']]
            print "temporary DF is done"

            if flag == 'I' or flag == 'i':
                print "Begin writing into ", FileToWriteTO
                if count == 0:
                    #FinalDF.to_csv("eMODIS.csv")
                    tempDF.to_csv(FileToWriteTO,index=False)
                else:
                    with open(FileToWriteTO,'a') as f:
                        #FinalDF.to_csv(f, header = false)
                        tempDF.to_csv(f, index = False, header = False)
                        f.close()
                print "Data written to ", FileToWriteTO
            elif flag == 'U' or flag == 'u':
                FileToWriteToUpdate = FileToWriteTo
                if count == 0:
                    print "Begin writing into ", FileToWriteToUpdate
                    tempDF.to_csv(FileToWriteToUpdate, index = False)
                    print "Data written to ", FileToWriteToUpdate
                else:
                    with open(FileToWriteToUpdate, 'a') as f:
                        tempDF.to_csv(f, index = False, header = False)
                        f.close()

            #step9.
            print 'remove the temporary .csv file and flush the staging folder'
            try:
                os.remove(csvFile)
            except:
                print 'Could not remove the file as some process is using it'

            files = glob.glob(downloadZipFileToPath+'*')
            for f in files:
                try:
                    os.remove(f)
                except:
                    print 'Could not remove the file as some process is using it'
            print 'loop ',count,' complete'
            print
            count = count + 1
            print "count: ", count
        else:
            print title
            files = glob.glob(downloadZipFileToPath+'*')
            for f in files:
                try:
                    os.remove(f)
                except:
                    print 'Could not remove the file as some process is using it'
            #print 'files removed'
            print 'count is reset to loopCount'
            count = loopCount
            print "count: ", count
            #raw_input("Please Hit Enter to proceed ")


if __name__ == '__main__':
    scrape(URLS, flag,FileToWriteTO)
    FlatFileToUpload(db, schema, sqlServerInstance, workingDir, FileToUpload,tableToWriteTo)
    loadUpdatedDataToDB(db, schema, sqlServerInstance, workingDir,UpdateFileToUpload, tableToWriteTo)
