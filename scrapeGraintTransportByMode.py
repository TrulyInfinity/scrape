##########################
######Date: 01-18-2016
######Author: Tridib, Lin
##########################
######Summary: This script batch read all the fixed width text files into CSV
###########################
import pandas as pd
import urllib



##retrieve the excel file (put in the code here)
url = "http://www.ams.usda.gov/sites/default/files/media/DATA%20FOR%20MODAL%20SHARE%20STUDY%202013.xlsx"
testFile = urllib.URLopener()
testFile.retrieve(url, "01_output.xlsx")



# these names are retrieved from 01_output.xlsx file above
xls = pd.ExcelFile("01_output.xlsx")
allSheetNames = xls.sheet_names

ExcelSheetName = allSheetNames[1:7]
#ExcelSheetName = ['ALL GRAINS BY MODE ', 'CORN BY MODE','WHEAT BY MODE','SOYBEANS BY MODE','SORGHUM BY MODE','BARLEY BY MODE']



def buildTable(SheetName, inputFileName = "01_output.xlsx"):

    ## if 'ALL GRAINS BY MODE ' is little bit different from the Grain worksheets themselves
    if SheetName == 'ALL GRAINS BY MODE ':
        DF = pd.read_excel('01_output.xlsx', sheetname = SheetName, skiprows=3)
    else:
        DF = pd.read_excel('01_output.xlsx', sheetname = SheetName, skiprows= 2)

    ## drop the columns 2,4,6
    dropcols = [2,4,6]
    DF.drop(DF.columns[[dropcols]], axis = 1, inplace = True)


    ## drop the oth row. not needed
    DF.drop(DF.index[[0]], inplace = True)
    DF.index = range(DF.shape[0]) #change the row index
    DF.head()


    Total = DF[:36]
    Export = DF[37: 67]
    Domestic = DF[68:]

    # reset the index (Total's index is already from 0)
    Export.index = range(Export.shape[0])
    Domestic.index = range(Domestic.shape[0])

    # rename the columns
    Total.columns = ['Year','Rail_Total_1000tons','Barge_Total_1000tons','Truck_Total_1000tons']
    Export.columns = ['Year','Rail_Export_1000tons','Barge_Export_1000tons','Truck_Export_1000tons']
    Domestic.columns = ['Year','Rail_Domestic_1000tons','Barge_Domestic_1000tons','Truck_Domestic_1000tons']

    ## merge the two columnwise to get one table (colnames indicates Export or Domestic or total)
    tempDF = pd.concat([Domestic,Export[[1,2,3]]], axis = 1)

    newDF = pd.merge(tempDF, Total, on= 'Year', how = 'outer', left_index = True)
    newDF = newDF.sort_index()
    fieldName = pd.Series(SheetName.split(" ")[0], index = range(newDF.shape[0]) )
    newDF = pd.concat([newDF, fieldName], axis = 1)

    return(newDF)

for i in range(len(ExcelSheetName)):
    if i == 0:
        tempDF = buildTable(ExcelSheetName[i])
    else:
        temp = buildTable(ExcelSheetName[i])
        tempDF = pd.concat([tempDF, temp])

tempDF.columns.values[len(tempDF.columns)-1] = 'GrainName'
tempDF.to_csv("GrainTransportByMode.csv", index = False)
