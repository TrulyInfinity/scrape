
# coding: utf-8

# In[1]:

import pandas as pd
import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')


# In[2]:

import matplotlib
matplotlib.style.use('ggplot')


# In[3]:

import seaborn as sns


# In[1098]:

df = pd.read_csv("us_census_full/census_income_learn.csv", header = None)


# In[1099]:

df.shape


# In[1100]:

df.info()


# In[1101]:

df.head()


# In[1102]:

df.iloc[:,41].unique().tolist()


# In[1103]:

df.describe()


# In[1104]:

def visualizeCatagoricalStat(df,n):
    NAratio = 0
    if df.dtypes.values[n] == 'O':
        #uniqFactors = df.iloc[:,n].unique().tolist()
        valCounts = df.iloc[:,n].value_counts().tolist()
        A = pd.Series(df.iloc[:,n].value_counts())
        if ' NA' in A.index:
            NAratio = A[' NA']/float(df.shape[0])
        sns.countplot(y=df.columns[n], data = df, palette = "Greens_d")
        return [A, NAratio]
    else:
        return ["col in not of type 'Object'",""]
        



# In[1105]:

coltypes = df.dtypes.values
coltypes


# ## Visualizing the categorical variables

# In[122]:

a,b = visualizeCatagoricalStat(df, 1)

print "NA ratio: ", b
print a


# In[123]:

a,b= visualizeCatagoricalStat(df, 4)


print "NA ratio: ", b
print a


# In[124]:

a,b= visualizeCatagoricalStat(df,6)

print "NA ratio: ", b
print a


# In[125]:

a,b= visualizeCatagoricalStat(df, 7)

print "NA ratio: ", b
print a


# In[126]:

a,b= visualizeCatagoricalStat(df, 8)

print "NA ratio: ", b
print a


# In[127]:

a,b=visualizeCatagoricalStat(df, 9)

print "NA ratio: ", b
print a


# In[128]:

a,b=visualizeCatagoricalStat(df, 10)

print "NA ratio: ", b
print a


# In[129]:

a,b= visualizeCatagoricalStat(df, 11)

print "NA ratio: ", b
print a


# In[130]:

a,b = visualizeCatagoricalStat(df, 12)

print "NA ratio: ", b
print a


# In[131]:

a,b = visualizeCatagoricalStat(df, 13)

print "NA ratio: ", b
print a


# In[132]:

a,b = visualizeCatagoricalStat(df, 14)

print "NA ratio: ", b
print a


# In[133]:

a,b = visualizeCatagoricalStat(df, 15)

print "NA ratio: ", b
print a


# In[134]:

a,b = visualizeCatagoricalStat(df, 19)

print "NA ratio: ", b
print a


# In[135]:

a,b = visualizeCatagoricalStat(df, 20)

print "NA ratio: ", b
print a


# In[136]:

a,b = visualizeCatagoricalStat(df, 21)

print "NA ratio: ", b
print a


# In[137]:

a,b = visualizeCatagoricalStat(df, 22)

print "NA ratio: ", b
print a


# In[138]:

a,b = visualizeCatagoricalStat(df, 23)

print "NA ratio: ", b
print a


# In[139]:

a,b = visualizeCatagoricalStat(df, 25)

print "NA ratio: ", b
print a


# In[140]:

a,b = visualizeCatagoricalStat(df, 26)

print "NA ratio: ", b
print a


# In[141]:

a,b = visualizeCatagoricalStat(df, 27)

print "NA ratio: ", b
print a


# In[142]:

a,b = visualizeCatagoricalStat(df, 28)

print "NA ratio: ", b
print a


# In[143]:

a,b = visualizeCatagoricalStat(df, 29)

print "NA ratio: ", b
print a


# In[144]:

a,b = visualizeCatagoricalStat(df, 31)

print "NA ratio: ", b
print a


# In[145]:

a,b = visualizeCatagoricalStat(df, 32)

print "NA ratio: ", b
print a


# In[146]:

a,b = visualizeCatagoricalStat(df, 33)

print "NA ratio: ", b
print a


# In[147]:

a,b = visualizeCatagoricalStat(df, 34)

print "NA ratio: ", b
print a


# In[148]:

a,b = visualizeCatagoricalStat(df, 35)

print "NA ratio: ", b
print a


# In[149]:

a,b = visualizeCatagoricalStat(df, 37)

print "NA ratio: ", b
print a


# In[150]:

a,b = visualizeCatagoricalStat(df, 41)

print "NA ratio: ", b
print a


# ## Visualizing the numerical variables 

# In[102]:

df.columns[coltypes == 'int64']


# In[99]:

df.iloc[:,0].plot(kind = 'box')


# In[105]:

df.iloc[:,2].plot(kind = 'hist')


# In[106]:

df.iloc[:,3].plot(kind = 'hist')


# In[109]:

df.iloc[:,5].plot(kind='hist', bins = 20)


# In[112]:

df.iloc[:,16].plot(kind='hist')


# In[113]:

df.iloc[:,17].plot(kind="hist")


# In[114]:

df.iloc[:,18].plot(kind="hist")


# In[115]:

df.iloc[:,30].plot(kind="hist")


# In[116]:

df.iloc[:,36].plot(kind="hist")


# In[117]:

df.iloc[:,38].plot(kind="hist")


# In[118]:

df.iloc[:,39].plot(kind="hist")


# In[120]:

df.iloc[:,40].plot(kind="hist")


# ### Basic statistics about the non-categorical variables

# In[984]:

df[df.columns[coltypes=='int64']].describe()


# ### Basic manipulation of the data

# Change the target variable (column 41 in the above df) to 0/1 (-50000/50000 +)

# In[1106]:

df.iloc[:,41].unique()


# In[1107]:

#keep it aside before modifying just in case we need it later
targetVar = df.iloc[:,41]

# modify

targetVar = df.iloc[:,41].apply(lambda x: 0 if x == ' - 50000.' else 1)

#drop this column

df = df.drop(df.columns[41], axis = 1)


# In[1108]:

sum(targetVar)


# In[1109]:

coltypes


# We will drop the NA's from the table. We noticed that column 11 has NAs which is about 0.04% of the total number of rows. Further, we calculate how many 1s (encoded as 50000+ ) is affected if we remove the NAs from our dataset

# In[1110]:

df.iloc[:,11].value_counts()


# We see that only 57 out of 12382 1s are affected, while 874-57 = 817 out of 187141 0s are affected. That is about  0.4% of each label is affected by the presence of NAs. So we decided to replace them with most frequent before proceeding with further processing and modeling

# In[1111]:

df.iloc[:,11] = df.iloc[:,11].replace(' NA', ' All other')


# In[1112]:

df.iloc[:,11].value_counts()


# Also noticed that the test set doesn't have ' Grandchild <18 ever marr not in subfamily' as a level in column 22. So we are going to drop it from our training set to be consistant (there are only two occurances of this in the column)

# In[1113]:

mask = df.iloc[:,22] != ' Grandchild <18 ever marr not in subfamily'


# In[1114]:

df = df.ix[mask,:]


# In[1115]:

targetVar = targetVar[mask]


# In[1116]:


print len(targetVar)
print df.shape


# In[1117]:

sum(df.iloc[:,22] == ' Grandchild <18 ever marr not in subfamily')


# In[1118]:

len(df.iloc[:,22].unique())


# In[1119]:

## replaces " ?" by 'UA' which stands for UnAvailable
new_df = df.replace(' ?',' UA')


# In[1120]:

typeList = new_df.dtypes.values


# In[1121]:

#OneHot Encoding

dummyVar = pd.DataFrame()

for i,t in enumerate(typeList):
    if t == 'O':
        one_hot = pd.get_dummies(new_df.iloc[:,i])
        one_hot.columns = [str(i)+x for x in one_hot.columns]
        dummyVar = pd.concat([dummyVar, one_hot], axis = 1)
        print i,' ', len(new_df.iloc[:,i].unique()),' ',one_hot.shape[1], '  ', dummyVar.shape[1]

        
        
        


# In[1122]:

#drop the cols of objects and concat the dummyVar dataframe with new_df
new_df = new_df.loc[:,new_df.columns[typeList != 'O']]

print new_df.shape

new_df = pd.concat([new_df, dummyVar], axis = 1)

print new_df.shape


# In[1123]:

new_df.columns


# In[1124]:

sum(new_df.dtypes.values == 'O')


# This shows that there aren't any column containing categorical variables.

# Need some cleaning of the columns generated by the above encoding

# In[1125]:

cols = pd.Series(new_df.columns)


# In[1126]:

#strip of the white space at the begining and end (if exists) and then replace any in-between white space by '_"
new_cols = [x.strip().replace(' ', '_') if isinstance(x, str) else x for x in cols.values]


# In[1127]:

new_df.columns = new_cols


# Because of the encoding process above, some of the column names could be duplicated

# In[1128]:

duplicateCols = new_df.columns.get_duplicates()


# In[1129]:

duplicateCols


# There is no duplicate columns

# In[1011]:

#cols=pd.Series(new_df.columns)
#for dup in duplicateCols: 
#    cols[new_df.columns == dup]=[dup+'_'+str(d) for d in range(sum(new_df.columns==dup))]


# In[1012]:

#new_df.columns = cols


# Below we now recheck to see if there are any columns left which are still categorical and then see what they are 

# In[1131]:

new_df.dtypes[new_df.dtypes == 'O']


# There are no variables which are categorical. So we proceed with the modeling

# ## Model Building

# ### Logistic regression 

# In[1132]:

from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split
from sklearn import metrics
from sklearn.cross_validation import cross_val_score


# In[1133]:

lr = LogisticRegression()
lr_fit = lr.fit(new_df, targetVar)

#ls.score(new_df, targetVar)


# In[1134]:

lr_fit.score(new_df, targetVar)


# In[1017]:

print 1-targetVar.mean()


# Logistic regression model is performing better than the baseline model where you predict 0 no matter what (because of the prevalence of 0 in the target variable)

# In[1018]:

scores = cross_val_score(lr, new_df, targetVar, scoring = 'accuracy', cv = 5)
print scores
print scores.mean()


# ### Decision Tree Classifier

# In[1056]:

from sklearn.tree import DecisionTreeClassifier

dtclf = DecisionTreeClassifier(random_state=123)
dtclf_fit = dtclf.fit(new_df, targetVar)

dt_scores = cross_val_score(dtclf, new_df, targetVar, scoring='accuracy', cv=5)
print dt_scores
print dt_scores.mean()


# It seems that the Decision Tree achieved a mean accuracy that is slightly less than the bseline model outlined above.

# ## Test Data

# In[1135]:

#load the test data into test_df

test_df = pd.read_csv("us_census_full/census_income_test.csv", header = None)


# In[1136]:

test_df.shape


# In[1137]:

test_df.info()


# In[1138]:

test_df.iloc[:,41].unique()


# In[1139]:

#first convert the target variable to 0/1 as in training set above

targetVar_test = test_df.iloc[:,41].apply(lambda x: 0 if x == ' - 50000.' else 1)


# In[1140]:

#drop this column
test_df = test_df.drop(test_df.columns[-1], axis=1)


# In[1141]:

## replaces " ?" by 'UA' which stands for UnAvailable
new_test_df = test_df.replace(' ?',' UA')


# In[1142]:

coltypes_test = new_test_df.dtypes.values


# In[1143]:

#below we check if there are any NAs 
for i in range(new_test_df.shape[1]):
    if coltypes_test[i] == 'O':
        if sum(new_test_df.iloc[:,i] == ' NA') != 0:
            print i,' ',sum(new_test_df.iloc[:,i] == ' NA')


# We notice that only the 12th column has NA and there are only 405 of them. Remove them with most frequent category

# In[1144]:

new_test_df.iloc[:,11].value_counts()


# In[1145]:

new_test_df.iloc[:,11] = new_test_df.iloc[:,11].replace(' NA', ' All other')


# In[1146]:

new_test_df.iloc[:,11].value_counts()


# In[1147]:

dummyVar_test = pd.DataFrame()

for i, t in enumerate(coltypes_test):
    if t == 'O':
        one_hot = pd.get_dummies(new_test_df.iloc[:,i])
        one_hot.columns = [str(i)+x for x in one_hot.columns]
        dummyVar_test = pd.concat([dummyVar_test, one_hot], axis = 1)
        print i,' ',len(new_test_df.iloc[:,i].unique()),' ',dummyVar_test.shape[1],'',pd.get_dummies(df.iloc[:,i]).shape[1]


# In[1148]:

a = new_test_df.iloc[:,22].unique().tolist()

b = df.iloc[:,22].unique().tolist()



# In[1149]:

list(set(b)-set(a)) 


# In[1150]:

#remove those cols which are encoded
new_test_df = new_test_df.loc[:, coltypes_test != 'O']



# In[1151]:

#add the dummy vars

new_test_df = pd.concat([new_test_df, dummyVar_test], axis =1)


# In[1152]:

new_test_df.shape


# In[1153]:

new_df.shape


# ### Prediction on test data

# In[1154]:

#For logistic model

lr_y_hat = lr.predict(new_test_df)

lr_fit.score(new_test_df, targetVar_test)


# In[1155]:

from sklearn.metrics import accuracy_score
accuracy_score(targetVar_test, lr_y_hat)


# In[1156]:

from sklearn.metrics import classification_report

print classification_report(targetVar_test, lr_y_hat)


# In[1157]:

#decision tree

dtclf_y_hat = dtclf.predict(new_test_df)

print accuracy_score(targetVar_test, dtclf_y_hat)


# In[1158]:

from sklearn.feature_selection import chi2
scores, pvalues = chi2(new_df, targetVar)

#print pvalues


# In[1159]:

result_df = pd.DataFrame()

result_df['colname'] = new_df.columns.tolist()
result_df['coeffs'] = lr_fit.coef_.tolist()[0]
result_df['pvalues'] = pvalues
#pd.DataFrame( new_df.columns.tolist(),lr_fit.coef_.tolist(), pvalues.tolist() )

result_df


# ### LASSO (to select best features)

# In[1160]:

from sklearn.linear_model import LassoCV

lasso_model = LassoCV(cv = 10)
lasso_fit = lasso_model.fit(new_df, targetVar)


# In[1161]:

lasso_result_df = pd.DataFrame(zip(new_df.columns.tolist(), lasso_fit.coef_))


# In[1164]:

bestvars_lasso_df = lasso_result_df.loc[lasso_result_df.iloc[:,1]>0,:]


# In[1165]:

bestvars_lasso_df


# According to lasso, the best features are age, capital gain/loss, stocks and migration code change are the most important predictor of whether the person can earn more than 50000 in a year

# In[1177]:

affluent_df = df.loc[targetVar == 1,:]


# In[1178]:

affluent_df.iloc[:,0].max()


# In[1179]:

affluent_df.iloc[:,0].min()


# In[1180]:

affluent_df.iloc[:,0].mean()


# In[1184]:

affluent_df.iloc[:,16].mean()


# In[1185]:

affluent_df.iloc[:,17].mean()


# In[1186]:

affluent_df.iloc[:,18].mean()


# In[1187]:

affluent_df.iloc[:,24].mean()


# In[ ]:



